<?php
class Animal
{
    private $family;
    private $food;
    public function __construct($family, $food)
    {
        $this->family = $family;
        $this->food = $food;
    }
    public function get_family()
    {
        return $this->family;
    }
    public function set_family($family)
    {
        $this->family = $family;
    }
    public function get_food()
    {
        return $this->food;
    }
    public function set_food($food)
    {
        $this->food = $food;
    }
}

class Cow extends Animal
{
    private $owner;
    public $public_owner;
    private $count = 10;
    public function __construct($family, $food)
    {
        parent::__construct($family, $food);
    }
    public function set_owner($owner)
    {
        $this->owner = "India ".$owner;
    }
    public function get_owner()
    {
        return "Mr ".$this->owner;
    }
    public function getNoanimals()
    {
        echo $this->count;
    }
}

class Lion extends Animal
{
   /* public function __construct($family, $food)
    {
        parent::__construct($family, $food);
    } */
}

$cow = new Cow('Herbivore', 'Grass');
$lion = new Lion('Canirval', 'Meat');
echo '<b>Cow Object</b> <br>';
echo 'The Cow kkkkk to the ' . $cow->get_family() . ' family and eats ' . $cow->get_food() . '<br><br>';
echo '<b>Lion Object</b> <br>';
echo 'The Lion aaaa to the ' . $lion->get_family() . ' family and eats ' . $lion->get_food(). '<br><br>';
$cow->set_owner("Shreyas");
echo "Owner".$cow->get_owner()."<br>";
$cow->public_owner="xyzzz";
echo "wowo  ".$cow->public_owner."<br>";
echo "CT  ".$cow->getNoanimals()."<br>";