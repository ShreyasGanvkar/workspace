<?php
include 'header.php';
include 'navbar.php';
?>
<!-------------------------------------------------------Hero----------------------------------------------------->
	<section>
		<div class="container-fluid cover">
			<div class="row h-100">
				<div class="col-md-7 col-xs-12 d-flex align-items-center">
					<div class="">
						<div class="row power-margin mobile-align mb-3">
							<div class="col-md-12">
								<h1 class="ms-3 text-white fw-bold">
									Your Partner In Power
								</h1>
							</div>
						</div>
						<div class="row mobile-align mb-3">
							<div class="col-md-12">
								<h5 class="ms-3 text-white fw-bold">
									We are here for all your Solar Power needs.
								</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 mt-2 mb-3">
								<div class="d-grid gap-2 d-md-block btn-group ms-md-3 btn-margin">
								  <a href="#" class="btn btn-primary border-0 hero-button" role="button">Get in touch with us!</a>
								  <a href="#" class="btn btn-primary border-0 hero-button" role="button">View our products</a>
								  <!--a href="#" class="btn btn-primary border-0 hero-button d-md-none" role="button">Services</a-->
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-md-5 h-100 d-none d-sm-block">
					<div class="my-5 mx-3">
						<div class="row">
							<div class="col-md-6" >
								<div class="card border-0 mb-3 custCard">
									<img src="images/jobc.jpg" class="img-fluid card-img-top card-img">
									<div class="card-img-overlay text-white fw-bold">
										<span class="services-line">
										Job Contracting
										</span>
									</div>	
								</div>
							</div>
							<div class="col-md-6" >
								<div class="card border-0 mb-3">
									<img src="images/card3.jpg" class="img-fluid card-img-top card-img">
									<div class="card-img-overlay text-white fw-bold">
										<span class="services-line">
										Contract Manufacturing
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="card border-0 mb-3">
									<img src="images/card2.jpg" class="img-fluid card-img-top card-img">
									<div class="card-img-overlay text-white fw-bold">
										<span class="services-line">
										OEM
										</span>
									</div>
								</div>
							</div>					
							<div class="col-md-6">
								<div class="card border-0">
									<img src="images/card1.jpg" class="img-fluid card-img-top card-img">
									<div class="card-img-overlay text-white fw-bold">
										<span class="services-line">
										Production
										</span>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>	
<!---------------------------------------------Services Mobile----------------------------------------------->	 
 <section class="services-mobile">	
		<div class="container-fluid card-display py-4">
			<div class="card p-2 shadow-sm">
				<div class="row mt-3 mb-2">
					<div class="col-md-12">
						<div class="page-header">
							<h1 class="text-center fw-bold">
								Our <span class="span-yellow">Services</span>
							</h1>
						</div>
					</div>
				</div>
				<a href="#" style="text-decoration: none; color: black;">
					<div class="card mb-2" style="background-color: #007ec3; color: #ffffff">
						<div class="row">
							<div class="col-md-12">
								<div class="card-body">
									<span class="card-title">Job Contracting</span>
									<i class="fa fa-arrow-circle-right float-end mt-1" aria-hidden="true"></i>
								</div>
							</div>
						</div>
					</div>
				</a>
				<div class="card mb-2 " style="background-color: #007ec3; color: #ffffff">
					<div class="row">
						<div class="col-md-12">
							<div class="card-body">
								<span class="card-title ">Contract Manufacturing</span>
								<i class="fa fa-arrow-circle-right float-end mt-1" aria-hidden="true"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="card mb-2" style="background-color: #007ec3; color: #ffffff">
					<div class="row">
						<div class="col-md-12">
							<div class="card-body">
								<span class="card-title">OEM</span>
								<i class="fa fa-arrow-circle-right float-end mt-1" aria-hidden="true"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="card " style="background-color: #007ec3; color: #ffffff">
					<div class="row">
						<div class="col-md-12">
							<div class="card-body">
								<span class="card-title ">Production</span>
								<i class="fa fa-arrow-circle-right float-end mt-1" aria-hidden="true"></i>
							</div>
						</div>
					</div>
				</div>
				<div class="row d-md-none">
					<div class="col-md-12 text-center">
						<a href="#" class="btn btn-block border-0 hero-button my-3">View All The Services</a>
					</div>
				</div>
			</div>	
		</div>
	</section>
<!------------------------------------------------About us----------------------------------------------->
<section class="about-why-cover pt-2">
	<section>
		<div class="container">
			<div class="card p-3 mt-3 mb-2 aboutus-card">
				<div class="row mt-2 mb-2">
					<div class="col-md-12">
						<div class="page-header">
							<h1 class="text-center fw-bold">
								About <span class="span-yellow">Us</span>
							</h1>
						</div>
					</div>
				</div>
				<div class="row mb-3">
					<div class="col-md-12 col-sm-12 text-center">
						<p>
							Maruthi Solar Systems was started in the year 1998. 
							We have been evaluated as ' A' Grade supplier based on our performance for the last many years by our customers like BHEL, BEL, TATA BP Solar & Reliance Industries.
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 text-center">
						<a href="#" class="btn btn-primary btn-block border-0 hero-button" role="button">Read more</a>
					</div>
				</div>
			</div>		
				<div class="row mt-5 d-sm-none d-md-inline">
					<div class="col-md-4 mb-3  d-none d-sm-inline-block">
						<div class="card card-border__yellow card-aboutus mx-auto">
							<div class="card-body text-center">
								<h5 class="card-title fw-bold">Our <span class="span-yellow">Vision</span></h5>
								<p class="card-text mt-3">To be a worldwide solar product provider to meet the global energy needs.</p>
							</div>	
						</div>
					</div>
					<div class="col-md-4 mb-3 d-none d-sm-inline-block">
						<div class="card card-border__yellow card-aboutus mx-auto ">
							<div class="card-body text-center">
								<h5 class="card-title fw-bold">Our <span class="span-yellow">Mission</span></h5>
								<p class="card-text mt-3">To Provide cost effective & reliable solar energy solutions.</p>
							</div>	
						</div>
					</div>
					<div class="col-md-4 mb-3 d-none d-sm-inline-block">
						<div class="card card-border__yellow card-aboutus mx-auto ">
							<div class="card-body text-center">
								<h5 class="card-title fw-bold">Our <span class="span-yellow">Services</span></h5>
								<p class="card-text">Design<br/>Production<br/>Contract Manufacturing<br/>OEM<br/>Job Contraction</p>
							</div>	
						</div>
					</div>
				</div>
		</div>
	</section>
	<section class="why-us-cover">
		<div class="container-fluid py-3">
			<div class="row">
			<div class="col-md-12 flexing">
			<div class="row h-100">
				<div class="col-md-6 flexing d-none d-sm-block">
					<div class="card card-team-us">
						<div class="card-body text-center">
							<h5 class="card-title mt-1 fw-bold">Our <span class="span-yellow">Team</span></h5>
							<p class="card-text">The company is managed by technically qualified, professionally experienced, Young and Dynamic Technocrats.The in- house Research and Development, helps in developing the products of high quality and reliability with optimum design and considerable reduction in cost. The quality being the most important parameter and with guaranteed after sales service...
							</p>
							<a href="#" class="btn btn-block border-0 hero-button mt-4">Read more</a>
						</div>	
					</div>
				</div>
				<div class="col-md-6 flexing">
					<div class="card card-team-us">
						<div class="card-body text-center">
							<h5 class="card-title mobile-text-bold fw-bold">Why <span class="span-yellow">Us?</span></h5>
							<p class="card-text">We have established a strong foothold in the domain due to the superior nature of the product range offered & our expertise in the solar industries for more than two decades.
							Our goods are in line with industry requirements and are available at the most reasonable prices. We select specialists who work long hours to manufacture goods according to the standards and specifics provided by our clients in order to satisfy...
							</p>
							<a href="#" class="btn btn-block border-0 hero-button mt-3">Read more</a>
						</div>	
					</div>
				</div>
			</div>
			</div>
			</div>
		</div>
	</section>
</section>	
<!----------------------------------------Some of our Products----------------------------------------------->	
	<!--section >
		<div class="container-fluid">
			<div class="row mt-5 mb-3 d-none d-sm-inline">
				<div class="col-md-12">
					<div class="page-header">
						<h1 class="text-center">
							Some Of Our <span class="span-yellow">Products</span>
						</h1>
					</div>
				</div>
			</div>
			<div class="row ">
				<div class="col-md-3 d-none d-sm-inline">
					<div class="row">
						<div class="col-md-12">
						<a href="#">
							<div class="card card-border__yellow">
								<img src="images/solar-street-light.jpg" class="card-img-top card-img">
								<div class="card-img-overlay text-center text-white flexing">
									<span class="span-yellow">
										Solar Street Light
									</span>
								</div>
							</div>
						</a>	
						</div>	
					</div>
				</div>
				<div class="col-md-3 d-none d-sm-inline">
					<div class="row">
						<div class="col-md-12">
						<a href="#">
							<div class="card card-border__yellow">
								<img src="images/charge-c-01.jpg" class="card-img-top card-img">
								<div class="card-img-overlay text-center text-white flexing">
									<span class="span-yellow">
										Solar Charge Controller
									</span>
								</div>
							</div>
						</a>	
						</div>	
					</div>
				</div>
				<div class="col-md-3 d-none d-sm-inline">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-border__yellow">
								<img src="images/solar_inveter.jpg" class="card-img-top card-img">
								<div class="card-img-overlay text-center text-white flexing">
									<span class="span-yellow">
										Solar Inverter
									</span>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<div class="col-md-3 d-none d-sm-inline">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-border__yellow ">
								<img src="images/solar-street-light.jpg" class="card-img-top card-img">
								<div class="card-img-overlay text-center text-white flexing">
									<span class="span-yellow">
										Solar Street Light
									</span>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
			<div class="row d-none d-sm-inline">
				<div class="col-md-12 text-center">
					<a href="#" class="btn btn-block border-0 hero-button my-3">View All The Products</a>
				</div>
			</div>
		</div>	
	</section-->		
<!----------------------------------------Some of our Products Mobile view----------------------------------->	
	<!--section style="background-color: #F7E7D4">
		<div class="container-fluid py-3">
			<div class="card d-md-none shadow-sm p-2">
				<div class="row mt-3 mb-3 d-md-none">
					<div class="col-md-12">
						<div class="page-header">
							<h1 class="text-center">
								Some Of Our <span class="span-yellow">Products</span>
							</h1>
						</div>
					</div>
				</div>
				<div class="card d-md-none mb-2 border-0">
					<div class="row">
						<div class="col-md-12">
							<img src="images/charge-c-01.jpg" style="width: 70px; height:60px">
							<span class="">Solar Charge Controllers</span>
						</div>	
					</div>
				</div>
				<div class="card d-md-none mb-2 border-0">
					<div class="row">
						<div class="col-md-12">
							<img src="images/solar-street-light.jpg" style="width: 70px; height:60px">
							<span class="">Solar Street Lighting</span>
						</div>	
					</div>
				</div>
				<div class="card d-md-none mb-2 border-0">
					<div class="row">
						<div class="col-md-12">
							<img src="images/solar_inveter.jpg" style="width: 70px; height:60px">
							<span class="">Solar Inverters</span>
						</div>	
					</div>
				</div>
				<div class="card d-md-none mb-2 border-0">
					<div class="row">
						<div class="col-md-12">
							<img src="images/charge-c-01.jpg" style="width: 70px; height:60px">
							<span class="">Solar Charge Controllers</span>
						</div>	
					</div>
				</div>
				<div class="row d-md-none">
					<div class="col-md-12 text-center">
						<a href="#" class="btn btn-block border-0 hero-button my-3">View All The Products</a>
					</div>
				</div>
			</div>	
		</div>
	</section-->
<!--------------------------------------------------Products Carousel---------------------------------------->	
	<main class="pt-1 pb-3">
	  <div class="container-fluid">
		<div class="row mt-3 mb-3">
			<div class="col-md-12">
				<div class="page-header">
					<h1 class="text-center fw-bold">
						Some Of Our <span class="span-yellow">Products</span>
					</h1>
				</div>
			</div>
		</div>
		<div id="slick">

		  <div class="slide">
			<div class="card border-0">
			  <img src="images/charge-c-01.jpg" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="card border-0">
			  <img src="images/charge-c-02.jpg" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="card border-0">
			  <img src="images/charge-c-03.jpg" class="carousel-card-img" />
			</div>
		  </div>

		   <div class="slide">
			<div class="card border-0">
			  <img src="images/charge-c-01.jpg" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="card border-0">
			  <img src="images/charge-c-02.jpg" class="carousel-card-img" />
			</div>
		  </div>

		  <div class="slide">
			<div class="card border-0">
			  <img src="images/charge-c-03.jpg" class="carousel-card-img" />
			</div>
		  </div>

		</div>
	  </div>
	</main>
<!-------------------------------------------Some of our projects------------------------------------------------>
<section class="projects-customer-cover pt-4">	
	<section>
		<div class="container-fluid pb-3">
		<div class="card p-2 mb-3 projects-card">
			<div class="row">
				<div class="col-md-12 my-2">
					<div class="page-header">
						<h1 class="text-center fw-bold">
							Some Of Our <span class="span-yellow">Projects</span>
						</h1>
					</div>
				</div>
			</div>
			<div class="row mt-4 mb-3">
				<div class="col-md-4 flexing mb-4">
					<div class="card border-0 card-customer-test">
						<img src="images/power-plant-1.jpg" class="card-img-top card-border__yellow p-1" alt="...">
						<div class="card-body text-center">
							<p class="card-text">3.5KW at RR Temple, Bangalore through BHEL, Bangalore</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card border-0 card-customer-test">
						<img src="images/power-plant-4.jpg" class="card-img-top card-border__yellow p-1">
						<div class="card-body text-center">
							<p class="card-text">42KW at ESD, BHEL, Bangalore thorough BHEL, Bangalore</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card border-0 card-customer-test">
						<img src="images/power-plant-6.jpg" class="card-img-top card-border__yellow p-1">
						<div class="card-body text-center">
							<p class="card-text">40KW at Engineering College, Kanchipuram through BHEL</p>
						</div>	
					</div>
				</div>
			</div>
			<div class="row mt-5 mb-3">
				<div class="col-md-12 text-center">
					<a href="#" class="btn btn-block border-0 hero-button">View All Of Our Projects</a>
				</div>
			</div>
		</div>	
		</div>
	</section>
<!---------------------------------------------------Customer Testimonials------------------------------------>	
	<section class="customer-test-cover">
		<div class="container-fluid d-none d-md-block">
			<div class="row">
				<div class="col-md-12 my-4">
					<div class="page-header">
						<h1 class="text-center fw-bold">
							<span class="span-white">Customer </span><span class="span-yellow">Testimonials</span>
						</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 flexing mb-4">
					<div class="card card-border__yellow card-customer-test">
						<div class="card-body text-center">
							<img src="images/bhel.gif" class="card-title">
							<p class="card-text">“In a single contact, we received detailed and timely assistance. For improved quality,
							I recommend this company and its professional engineers.”</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card card-border__yellow card-customer-test">
						<div class="card-body text-center">
							<img src="images/reliance-solar-group.gif" class="card-title">
							<p class="card-text">“I always say that the quality and service at this organisation are fantastic, and this is just another example of the positive work you all do there.”</p>
						</div>	
					</div>
				</div>
				<div class="col-md-4 flexing mb-4">
					<div class="card card-border__yellow card-customer-test">
						<div class="card-body text-center">
							<img src="images/bharat-electronics.gif" class="card-title">
							<p class="card-text">“This team provides exceptional service and assistance both before and after the purchase.”</p>
						</div>	
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<a href="#" class="btn btn-block border-0 hero-button my-3">Read More Customer Testimonials</a>
				</div>
			</div>
		</div>
	</section>
</section>	
<!-----------------------------------------------------Form------------------------------------------------------------->		
	<!--section>
		<div class="container-fluid req-form p-4">
			<div class="row">
				<div class="col-md-12">
					<div class="page-header my-4">
						<h1 class="text-center fw-bold">
							<span class="req-form_header">Tell Us What You Are Looking For?</span>
						</h1>
					</div>
				</div>
			</div>
			<div class="row flexing">
				<div class="col-md-12 col-sm-12 flexing req-form_jumbotron mb-5">
					<form class="pt-5 req-form_main">
						<div class="row">
						  <div class="col-md-12">
							<textarea class="form-control mb-3" id="exampleFormControlTextarea1"  rows="3" placeholder="Describe your requirements"></textarea>
						  </div>
						</div>
						<div class="row g-3">
						  <div class="col-md-6">
							<input type="text" class="form-control" placeholder="Enter your name" aria-label="First name">
						  </div>
						  <div class="col-md-6">
							<input type="text" class="form-control" placeholder="Enter your number" aria-label="Last name">
						  </div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<a href="#" class="btn btn-block border-0 hero-button my-3">Send Enquiry</a>
							</div>
						</div>
					</form>
				</div>
			</div>	
		</div>
	</section-->
	
<!-----------------------------------------------------Contact---------------------------------------------------->
	<!--section>
		<div class="container-fluid find-us-cover p-3">
			<div class="row">
				<div class="col-md-12">
					<div class="page-header">
						<h1 class="text-center fw-bold">
							<span class="span-yellow">Find Us</span>
						</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center contact-address">
					<h4>Mr. B. H Nayak</h4>
					<h4>Maruthi Solar Systems</h4>
					<h4># 41,42,43, Sy. No. 10/1
						Abbigere Main Road
						Near Bus Stop, Kereguddadahalli
					</h4>
					<h4>Bangalore - 560090.</h4>
				</div>
			</div>
			<div class="row text-center contact-no mt-3">
				<div class="col-md-4">
					<h3>+91 1234567890</h3>
				</div>
				<div class="col-md-4">
					<h3><a href="#" class="contact-no_anchor">Send SMS</a></h3>
				</div>
				<div class="col-md-4">
					<h3><a href="#" class="contact-no_anchor">Send Email</a></h3>
				</div>
			</div>
		</div>
	</section-->
<?php
include 'footerbar.php';
include 'footer.php';

?>
<script>
var breakpoint = {
  // Small screen / phone
  sm: 576,
  // Medium screen / tablet
  md: 768,
  // Large screen / desktop
  lg: 992,
  // Extra large screen / wide desktop
  xl: 1200
};

// page slider
$('#slick').slick({
	autoplay: true,
  autoplaySpeed: 0,
  draggable: true,
  infinite: true,
  dots: false,
  arrows: false,
  speed: 5000,
  mobileFirst: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: breakpoint.sm,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: breakpoint.md,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: breakpoint.lg,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: breakpoint.xl,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 5
      }
    }
  ]
});

</script>	

