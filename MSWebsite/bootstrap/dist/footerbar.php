<!-----------------------------------------------------Footer----------------------------------------------------->
	<footer class="footer">
		<div class="container-fluid">
			<div class="row py-5">
				<div class="col-md-3 text-center">
					<a class="brand fs-2" href="#">
					  <img src="images/logo.png" alt="">
					</a>
				</div>
				<div class="col-md-3 mt-2 mt-md-0 text-center">
					<h4 class="span-yellow">Contact</h4>
					<h6>+91 1234567890</h6>
					<h6>info@maruthisolar.com</h6>
				</div>
				<div class="col-md-2 mt-2 mt-md-0 text-center">
					<h4 class="span-yellow">Follow</h4>
					<h6>LinkedIN</h6>
					<h6>Facebook</h6>
					<h6>Instagram</h6>
				</div>
				<div class="col-md-2 mt-2 mt-md-0 text-center">
					<h4 class="span-yellow">About Us</h4>
					<h6>Products</h6>
					<h6>Services</h6>
					<h6>Clients</h6>
					<h6>Contact Us</h6>
				</div>
				<div class="col-md-2 mt-2 mt-md-0 text-center">
					<h4><span class="span-yellow">Next: </span>About Us</h4>
				</div>
			</div>
		</div>
	</footer>
