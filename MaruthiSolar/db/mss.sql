-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 31, 2020 at 07:44 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mss`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `adid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`adid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adid`, `username`, `password`) VALUES
(1, 'admin', 'qwerty');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `cy_id` int(50) NOT NULL AUTO_INCREMENT,
  `cy_name` varchar(255) NOT NULL,
  `cy_desc` text NOT NULL,
  PRIMARY KEY (`cy_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cy_id`, `cy_name`, `cy_desc`) VALUES
(1, 'Solar Charge Controllers', 'Maruthi’s Solar Charge Controller product lines are the best option for compact, stand alone solar systems. This charge controller\'s centerpiece is a microprocessor which controls a security and information functions as well as guaranteeing optimal charge control.'),
(2, 'Street Lighting System', 'MARUTHI\'s solar street lighting systems are designed to operate as one integrated system. This system includes the power generation, storage and management (solar panels, battery & controller) as well as the light itself, support frame & weatherproof housing.'),
(3, 'Solar Inverters', 'Maruthi’s Solar Inverter can power domestic loads like Personal Computers, Laptop, Incandescent bulbs, CFL lamps, Fluorescent lamps, Halogen lamps, Fans, small mixers, blenders, food processors, TV, music system, DVD player etc.'),
(4, 'MPPT', 'description'),
(5, 'Home Lighting System', 'MARUTHI\'s Solar home lighting systems are designed to operate as one integrated system. This system includes the power generation, storage and management (solar panels, battery & controller) as well as the light itself, support frame & attractive indoor lights and provision for connecting other loads (radio, tape recorder, portable TV, fan, etc... depending upon the system configuration) as well as a complete set of installation hardware.\r\n'),
(6, 'Stabilizer', 'descriptiondescriptiondescription');

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

DROP TABLE IF EXISTS `form`;
CREATE TABLE IF NOT EXISTS `form` (
  `fid` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `dsgn` text NOT NULL,
  `org` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `city` text NOT NULL,
  `phno` bigint(20) NOT NULL,
  `req` text NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`fid`, `name`, `dsgn`, `org`, `email`, `city`, `phno`, `req`) VALUES
(2, 'Shreyas', 'student', 'student', 'shreyasganvkar2052@gmail.com', 'mumbai', 7899149177, 'solar charge ontrollers - 10 pcs'),
(6, 'Rock', 'student', 'student', 'sdsds@ssdsds', 'asdsa', 7899149177, 'asdasd'),
(8, 'Shreyas', 'sas', 'asas', 'asas@sdsd', 'adasd', 789456, 'qwqwqwwqw'),
(9, 'Shreyas', 'sas', 'asas', 'asas@sdsd', 'adasd', 789456, 'qwqwqwwqw'),
(10, 'Shreyas', 'sas', 'asas', 'asas@sdsd', 'adasd', 789456, 'qwqwqwwqw'),
(11, 'Shreyas', 'sas', 'asas', 'asas@sdsd', 'adasd', 789456, 'qwqwqwwqw');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `pr_id` int(50) NOT NULL AUTO_INCREMENT,
  `pr_name` varchar(255) NOT NULL,
  `pr_desc` text NOT NULL,
  `pr_img` varchar(100) NOT NULL,
  `pr_feat` text NOT NULL,
  `pr_pdf` text NOT NULL,
  `pr_cat` int(50) NOT NULL,
  PRIMARY KEY (`pr_id`),
  KEY `pr_cat` (`pr_cat`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`pr_id`, `pr_name`, `pr_desc`, `pr_img`, `pr_feat`, `pr_pdf`, `pr_cat`) VALUES
(1, 'PWM Solar Charge Controller', 'Maruthi’s Solar Charge Controller product lines are the best option for compact, stand alone solar systems. This charge controller\'s centerpiece is a microprocessor which controls a security and information functions as well as guaranteeing optimal charge control. Separate LED\'s clearly show the battery\'s charge level. This means the operator can determine the current operating status of their system at any time. With their 3-stage charging process (boost, float, trickle), Maruthi Solar charge controller can also charge flooded lead acid and VRLA batteries. In order to maximize the battery\'s life further still, the charge controllers also feature integrated temperature compensation. The housing can be installed directly onto a wall.', 'imgs/01.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 1),
(2, 'Priority Solar Charge Controller', '', 'imgs/03.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 1),
(3, 'MOSFET version MPPT', '', 'imgs/14.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 4),
(4, 'IGBT version MPPT', '', 'imgs/13.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 4),
(5, 'PWM Solar Inverter', '', 'imgs/28.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 3),
(6, 'MPPT Solar Inverter', '', 'imgs/28.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 3),
(7, 'Tilting with wall mounting', '', 'imgs/4.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 5),
(8, 'Tube light', '', 'imgs/10.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 5),
(9, 'Bulb', '', 'imgs/12.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 5),
(10, 'Mini home light kit', '', 'imgs/17.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 5),
(11, 'Integrated Street Lighting System', '', 'imgs/21.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 2),
(12, '2-in-1 Solar Street Lighting System', '', 'imgs/25.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 2),
(13, 'Solar Street Light Luminary Drivers only', '', 'imgs/101.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 2),
(14, 'Servo Stabilizer', '', 'imgs/solar_inverter.jpg', 'Advanced microcontroller technology.Accurate voltage settings.Battery 12/24V auto detection.PWM based series battery charging.Very low voltage drops.Various LED’s to indicate battery status and faults.Push button controlled status indication to avoid idle power loss.100% solid state.High efficiency series regulation with three stage charging technique.Built-in temperature compensation.', 'pdf/UserManualcc.pdf', 6);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
