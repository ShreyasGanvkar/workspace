<?php require('include/header.php') ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
		        <div class="jumbotron">
			        <h3 class="text-center">About Us</h3>
                </div>
            </div>
        </div>
	</div>
	<!--Chnagesss-->
    <div class="container-fluid">
        <div class="row" style="margin-top:20px;">
            <div class="col-md-12">
                <p class="text-center" style="font-size:19px;">The company is managed by technically qualified, professionally experienced, Young and Dynamic Technocrats. The in- house Research and Development, helps in developing the products of high quality and reliability with optimum design and considerable reduction in cost. The quality being the most important parameter and with guaranteed after sales service of our products have helped us to grow to a considerable level. The Maruthi Solar Systems is a company with fifty lakhs of rupee turn-over, has carried out the major works related solar Photovoltaic systems.</p>
            </div>
        </div>
        <div class="row" style="margin-top:30px;">
           <div class="col-md-12"> 
                <h3 class="text-center" style="color:#007BFF">MAJOR PROJECTS UNDERTAKEN</h3>
                <p class="text-center" style="font-size:19px;margin-top:20px;">Designed, Manufactured, Installed and commissioned more than five hundred numbers of solar Powered Street Lighting systems in rural part of the Karnataka State. Designed and developed Solar Powered Lantern as per Ministry of Non- Conventional Energy Sources New-Delhi specifications and supplied to M/s Bharat Electronics, Bangalore of around ten thousand numbers.

                Designed and developed Solar Powered Street Lighting systems as per Ministry of Non- conventional Energy Sources, New Delhi specifications and supplied to M/s Bharat Electronics, Bangalore and Bharat Heavy Electricals Limited, Bangalore of more than three thousand numbers.

                Designed, developed, Manufactured & supplied solar Powered Traffic Blinkers to M/s Bharat Heavy Electrical limited, Bangalore.

                Designed and developed Solar/ mains hybrid power supplies for fixed wireless terminals as per Department of Telecommunication specifications and supplied to DOT suppliers.

                Designed, developed and supplied Solar/ mains hybrid power plant to Karnataka Police Department around 300 nos. for computer application.

                We are proud to convey that we are OE Manufactures for M/s Bharat Electronics, Bangalore, Bharat Heavy Electrical Limited, Bangalore and major Solar PV manufacturing Companies in Bangalore. Apart from the above Non-conventional power products, more than 2000 numbers of OFF-LINE UPS systems of various capacities ranging from 250 VA to 2 KVA have been manufactured and installed in and around Bangalore and other districts in Karnataka.

                Our company has been evaluated as ‘A’ grade supplier based on our performance for the last four to five years by our customers like BHEL, BEL, TATA BP SOLAR and SHELL SOLAR.</p>
                </div>   
        </div>
        <div class="row" style="margin-top:30px;">       
            <div class="col-md-12">
                <h3 class="text-center" style="color:#007BFF">ORGANISATION</h3>
                <p class="text-center" style="font-size:19px; margin-top:20px;">The company has a core team with eight qualified technical staff at various levels and with support for various ancillary industries for assembly, wiring, supply of raw materials, fabrication of sheet metal works, etc. The man power is upgraded from time to time, to provide uninterrupted services to our clients.

                    The testing and final quality   inspections are carried out in-house, to ensure high quality and reliable product. PCB CAD package is applied for all our design work related to PCB circuits.

                    The company is equipped with in-house test equipment’s like oscilloscope, multimeter, power supplies, auto transformer, SPV panels, Rainfall simulation test set up for luminaries, cyclic test facility and burn-in test facility. Documentation work is computerized for efficient organizing and good presentation.</p>
            </div>       
        </div>            
    </div>
<?php require('include/footer.php') ?>    