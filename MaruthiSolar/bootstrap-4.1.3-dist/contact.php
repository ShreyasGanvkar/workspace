<?php
require('include/header.php');
require_once 'classContact.php';
require_once 'db_config.php';
$insert = new Contact($conn);
if(isset($_POST['submit']))
    {
    $name = $_POST['nme'];
    $designation = $_POST['dsg'];
    $organisation = $_POST['org'];
    $email = $_POST['email'];
    $city = $_POST['city'];
    $phno = $_POST['phno'];
    $requirements =$_POST['req'];

    $insert->addInfo($name,$designation,$organisation,$email,$city,$phno,$requirements);
    echo '<script language="javascript">';
    echo 'alert("Submitted");';
    echo '</script>';
        
    }
    ?>
    <div class="container-fluid">
        <div class=row>
            <div class="col-md-12">
		        <div class="jumbotron">
			        <h3 class="text-center">Contact Us</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 text-center" >
                <address>
					<strong>Maruthi Solar Systems</strong></br>
					# 41,42,43, Sy. No. 10/1<br>
					Abbigere Main Road<br>
					Kereguddadahalli<br>
					Bangalore - 560090<br>
					<abbr title="Phone">P:</abbr> +91-80-23253889
				</address>
            </div>
            <div class="col-md-8">
             <div class="jumbotron" style="padding:20px">
                 <h3>Enquiry Form</h3>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" name="nme" required>
                    </div>
                    <div class="form-group">
                        <label for="dsg">Designation:</label>
                        <input type="text" class="form-control" id="dsg" name="dsg" required>
                    </div>
                    <div class="form-group">
                        <label for="org">Organization:</label>
                        <input type="text" class="form-control" id="org" name="org" required>
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail:</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="city">City:</label>
                        <input type="text" class="form-control" id="city" name="city" required>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone no.:</label>
                        <input type="tel" pattern="[0-9]{10}" placeholder="1234567890" class="form-control" id="phno" name="phno" required>
                    </div>
                    <div class="form-group">
                        <label for="requirements">Requirements:</label>
                        <input type="text" class="form-control" id="req" name="req" required>
                    </div>
                    <button type="submit" class="btn btn-primary" name="submit"  >Submit</button>
                    <button type="reset" class="btn btn-primary">Clear</button>
                </form>
            </div>  
            </div>    
        </div>
    </div>
   
<?php require('include/footer.php') ?>   