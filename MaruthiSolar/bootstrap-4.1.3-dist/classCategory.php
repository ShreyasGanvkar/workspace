<?php
//Category.php
class Category
{
    protected $conn;
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function fetchData($sql)
    {
        //error_log("fetch dta");
        //error_log($sql);
        $sql_stmt = $this->conn->prepare($sql);
        $sql_stmt->execute();
        return $sql_stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function iudData($sql)
    {
        $sql_stmt = $this->conn->prepare($sql);
        $sql_stmt->execute();
    }

    public function getCategories()
    {
        $sql = "SELECT cy_id,cy_name FROM category";
        return $this->fetchData($sql);

    }

    public function addCategories($catname, $catdesc)
    {
        $sql = "INSERT INTO `category`(`cy_name`, `cy_desc`) VALUES ('$catname','$catdesc')";
        $this->iudData($sql);
    }

    public function viewCategories()
    {
        $sql = "SELECT * FROM category";
        return $this->fetchData($sql);
    }

    public function editCategories($catID, $catNm, $catDesc)
    {
        $sql = "UPDATE `category` SET `cy_name`= '$catNm',`cy_desc`='$catDesc' WHERE `cy_id`=$catID ";
        $this->iudData($sql);
    }
}

?>
