<?php
class Products
{
    protected $conn;
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function fetchData($sql)
    {
        //error_log("fetch dta");
        //error_log($sql);
        $sql_stmt = $this->conn->prepare($sql);
        $sql_stmt->execute();
        return $sql_stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function iudData($sql)
    {
        $sql_stmt = $this->conn->prepare($sql);
        $sql_stmt->execute();
    }

    public function getProducts()
    {

        $sql = "SELECT * FROM products";
        return $this->fetchData($sql);
    }

    public function viewProduct($prId)
    {
        $sql = "SELECT * FROM `products` WHERE `pr_id` = $prId ";
        return $this->fetchData($sql);
    }

    public function getPrcat($catId)
    {

        $sql = "SELECT * FROM `products` WHERE `pr_cat`= $catId";
        return $this->fetchData($sql);
    }

    public function addProducts($prodName, $desc, $imgDst, $prFeat, $pdfDst, $category)
    {
        $sql = "INSERT INTO `products`(`pr_name`, `pr_desc`, `pr_img`, `pr_feat`, `pr_pdf`, `pr_cat`) VALUES ('$prodName','$desc','$imgDst','$prFeat','$pdfDst','$category')";
        $this->iudData($sql);

    }

    public function editProduct($pId, $pNm, $pDesc, $imgDst, $pFeat, $pdfDst, $cat)
    {
        $sql = "UPDATE `products` SET `pr_name`='$pNm',`pr_desc`='$pDesc',`pr_img`='$imgDst',`pr_feat`='$pFeat',`pr_pdf`='$pdfDst',`pr_cat`='$cat' WHERE `pr_id`=$pId ";
        $this->iudData($sql);
    }

    public function delProduct($pid)
    {
        $sql = "DELETE FROM `products` WHERE `pr_id`=$pid ";
        $this->iudData($sql);
    }
}

?>
