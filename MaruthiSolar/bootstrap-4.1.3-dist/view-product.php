
<?php require('include/header.php');
require_once 'db_config.php';
require_once 'classProduct.php';
$prId= $_GET['id'];
$obj = new Products($conn);
$data= $obj -> viewProduct($prId); 
foreach($data as $key => $value)
    {
        ?>
<div class="container-fluid">
		<div class="jumbotron">
			<h3 class="text-center"><?php echo $value['pr_name'] ?></h3>
		</div>
</div>
<!--Horizontal Card-->
<div class="container-fluid">
    <div class="card mb-3" style="max-width: 100%; border: none;">
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="<?php echo $value['pr_img'] ?>" class="card-img" alt="...">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                <h5 class="card-title"><?php echo $value['pr_name'] ?></h5>
                <p class="card-text"><?php echo $value['pr_desc'] ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<!--END-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
        <figure class="figure">
            <a href="admin/<?php echo $value['pr_pdf'] ?>" target="_blank" ><img class="figure-img img-fluid mx-auto d-block" src="imgs/pdf-icon.png" style="width:200px; height: 200px;" alt="pdf"></a>
            <figcaption class="figure-caption" style="text-align: center">Click on the image above to download the user manual</figcaption>
        </figure>
        </div>
        <div class="col-md" style="padding-left: 30px">
            <ul class="list-unstyled">
                <?php
                    $feat = $value['pr_feat'];
                    $pieces= explode('.', $feat);
                    foreach($pieces as $id)
                        {
                ?>
                <li><?php echo $id ?></li>
                        <?php } ?>
            </ul>
        </div>
    </div>
</div>
    
    <?php } require('include/footer.php'); ?>