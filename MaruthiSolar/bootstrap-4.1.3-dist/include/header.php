<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Solar Charge Controllers, Inverters and more | Maruthi Solar Systems Bengaluru, Karnataka</title>
	<meta name="description" content="Our strength lies in System design, Product development, Sourcing, Manufacturing, Installation & Commissioning of Solar Photovoltaic Systems.">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/base.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
	
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js">
	</script> <![endif]-->
	<script>
	   function load() {
        console.log("load event detected!");
		var textWrapper = document.querySelector('.ml12');
		textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
      }
      window.onload = load;
	  
		

anime.timeline({loop: true})
  .add({
    targets: '.ml12 .letter',
    translateX: [40,0],
    translateZ: 0,
    opacity: [0,1],
    easing: "easeOutExpo",
    duration: 1200,
    delay: (el, i) => 500 + 30 * i
  }).add({
    targets: '.ml12 .letter',
    translateX: [0,-30],
    opacity: [1,0],
    easing: "easeInExpo",
    duration: 1100,
    delay: (el, i) => 100 + 30 * i
  });
	
	</script>
	<style>
	.ml12 {
  font-weight: 200;
  font-size: 1.8em;
  text-transform: uppercase;
  letter-spacing: 0.5em;
}
	.ml12 .letter {
  display: inline-block;
  line-height: 1em;
}
	</style>
</head>
<body>
<div class="container-fluid">
	<div class=row>
		<div class="col-md-12">
			<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
				<a class="navbar-brand" href="index.php"><img src="imgs/lg.png" class="img-responsive" alt="LOGO"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>	
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item"><a class="nav-link" href="index.php" style="color:#fff">Home</a></li>
							<li class="nav-item"><a class="nav-link" href="aboutus.php" style="color:#fff">About Us</a></li>
							<!--<li class="nav-item"><a class="nav-link" href="#clients">Clients</a></li>-->
							<li class="nav-item"><a class="nav-link" href="products.php" style="color:#fff">Products</a></li>
							<!--<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="products.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Products</a>
								<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
									<li><a class="dropdown-item">Solar Inverter</a></li>
									<li><a class="dropdown-item">MPPT Charge Controller</a></li>
									<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">PWM Charge Controllers</a>
										<ul class="dropdown-menu">
											<li><a class="dropdown-item" href="#">.</a></li>
											<li><a class="dropdown-item" href="#">.</a></li>
										</ul>
									</li>
									<li><a class="dropdown-item">Solar Street Lights</a></li>
									<li><a class="dropdown-item">ACDB / DCDB / AJB / MJB</a></li>
								</ul>-->
							</li>
							<li class="nav-item"><a class="nav-link"href="contact.php" style="color:#fff">Contact Us</a></li>
						</ul>	
					</div>
			</nav>
		</div>
	</div>
</div>	