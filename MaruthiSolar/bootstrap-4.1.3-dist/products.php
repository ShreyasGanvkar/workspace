<?php require('include/header.php');
require_once 'db_config.php';
require_once 'classProduct.php'; ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="jumbotron">
					<h3 class="text-center">Our Products</h3>
					<h6 class="text-center">We offer various power solutions for your needs.</h6>
				</div>
			</div>	
		</div>	
	
		<div class="row">
			<div class="col-md-3">
				<div class="card  h-100">
				<div class="card-body">
					<nav class="navbar">
						<!-- Links -->
						<ul class="navbar-nav" id="catlist">
							<li class="nav-item">
								<a class="nav-link" href="#" onclick='allpro()')>All Products</a>
							</li> 		
						</ul>
					</nav>
				</div>	
				</div>
				<script>
				$(document).ready(function(){
					$.ajax({
							type: "GET",
							url: "http://localhost/myprojects/MaruthiSolar/bootstrap-4.1.3-dist/controller.php?doaction=getcategories",
							data: {},
                    		contentType: "application/json; charset=utf-8",
                    		dataType: "json",                    
                    		cache: false,                       
                    		success: function(response) 
							{	console.log(response);
								var cathtml = '';
								$.each(response, function(i, category)
									{ 	
										cathtml += "<li class='nav-item'><a class='nav-link' href='#' id='' onclick='forward("+category.cy_id+")' > "+category.cy_name+"</a></li>";
									});
								//console.log(cathtml);	
								$('#catlist').append(cathtml);
							},
							
						});
				});
				</script>
			</div>
			<div class="col-md-9">
				<div class="row" id="allProducts">
					
				</div>
				<script>
				function allpro(){
					$.ajax({
							type: "GET",
							url: "http://localhost/myprojects/MaruthiSolar/bootstrap-4.1.3-dist/controller.php?doaction=getproducts",
							data: {},
                    		contentType: "application/json; charset=utf-8",
                    		dataType: "json",                    
                    		cache: false,                       
                    		success: function(response) 
							{	console.log(response);
								var allProducts = '';
								$.each(response, function(i, product)
									{ 	
										allProducts += "<div class='col-md-4'><div class='card-deck'><div class='card h-100' style='width: auto; margin-bottom:15px;'><img class='card-img-top' src='"+product.pr_img+
										"' alt='"+product.pr_name+"'><div class='card-body'><h6 class='card-title' style='text-align: center'>"+product.pr_name+
										"</h6><a href='view-product.php?id="+product.pr_id+"' id='"+product.pr_id+
										"' name='submit' class='btn btn-primary btn-block' >View</a></div></div></div></div>";
									});
								//console.log(allProducts);
								$('#allProducts').html(allProducts);

							},
							
						});
				};
				</script>

				<script>
               $(document).ready(function(){
					$.ajax({
							type: "GET",
							url: "http://localhost/myprojects/MaruthiSolar/bootstrap-4.1.3-dist/controller.php?doaction=getproducts",
							data: {},
                    		contentType: "application/json; charset=utf-8",
                    		dataType: "json",                    
                    		cache: false,                       
                    		success: function(response) 
							{	console.log(response);
								var allProducts = '';
								$.each(response, function(i, product)
									{ 	
										allProducts += "<div class='col-md-4'><div class='card-deck'><div class='card h-100' style='width: 18rem; margin-bottom:15px;'><img class='card-img-top' src='"+product.pr_img+
										"' alt='"+product.pr_name+"'><div class='card-body'><h6 class='card-title' style='text-align: center'>"+product.pr_name+
										"</h6><a href='view-product.php?id="+product.pr_id+"' id='"+product.pr_id+
										"' name='submit' class='btn btn-primary btn-block' >View</a></div></div></div></div>";
									});
								//console.log(allProducts);
								$('#allProducts').append(allProducts);

							},
							
						});
				});	
				
                </script>
			</div>
		</div> 
</div>
<script>
	function forward(catid){
	 $.ajax({
                    type: "GET",
                    url: "http://localhost/myprojects/MaruthiSolar/bootstrap-4.1.3-dist/controller.php?doaction=getprcat&catId="+catid,
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",                    
                    cache: false,                       
                    success: function(response) {        
						console.log(response);
                        var cat = '';
                            $.each(response, function (i, prcat) {
								
                                cat += "<div class='col-md-4'><div class='card-deck'><div class='card h-100' style='width: 18rem; margin-bottom:15px;'><img class='card-img-top' src='"+prcat.pr_img+
										"' alt='"+prcat.pr_name+"'><div class='card-body'><h6 class='card-title' style='text-align: center'>"+prcat.pr_name+
										"</h6><a href='view-product.php?id="+prcat.pr_id+"' id='"+prcat.pr_id+
										"' name='submit' class='btn btn-primary btn-block' >View</a></div></div></div></div>";
                            });
							//console.log(cat);
                            $('#allProducts').html(cat);
                    },
                   
            }); 	
		};


	
	</script>

<?php require('include/footer.php') ?>
