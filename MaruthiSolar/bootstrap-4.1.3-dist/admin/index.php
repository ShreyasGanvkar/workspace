<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin Panel</title>
        <meta name="description" content="Admin Content">
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/base.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!--<script src="js/bootstrap.js"></script>
        <script src="js/jquery-3.4.1.js"></script>-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js">
        </script> <![endif]-->
    </head>
    <body>
        <div class="row">
            <div class="col-md-12">
                <form class="mx-auto" action="validate.php" method="post" style="width:50%">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text-box" class="form-control"  placeholder="Enter User Name" name="uName">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control"  placeholder="Enter Password" name="uPwd">
                    </div>
                    <button type="submit" class="btn btn-primary" name='submit'>Submit</button>
                </form>
            </div>
        </div>        
    </body>
</html>

