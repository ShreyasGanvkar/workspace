<?php
//	session_start();
  require_once '../db_config.php';
  require_once '../classCategory.php';
  require_once 'classChecklogin.php';
  
$val = new checkLogin($conn);
$check = $val->loginCheck();

$obj= new Category($conn);
$data= $obj->getCategories();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Add Products</title>
	<meta name="description" content="Admin content">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../css/base.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js">
	</script> <![endif]-->
</head>
<body>
    <div class="container-fluid">
    <h2><a href="adminPanel.php">Admin Panel</a></h2>
        <div class="row">
            <div class="col-md-3">
                <ul class="nav flex-column">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="#">Products</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="view-products.php">View Products</a>                          
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="#">Categories</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="addCategories.php">Add Categories</a>
                            <a class="dropdown-item" href="view-category.php">View Categories</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="#">Form</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="view-form.php">View form</a>
                        </div>
                    </li>
                    <li class="nav-item"><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <div class="col-md">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="productName">Product Name:</label>
                        <input type="text-box" class="form-control" name="prName">
                    </div>
                    <div class="form-group">
                        <label for="prDesc">Product Description:</label>
                        <textarea class="form-control" rows="5" name="prDesc"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="prFeat">Product Features:</label>
                        <textarea class="form-control" rows="5" name="prFeat"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="File">Product Pdf</label>
                        <input type="file" class="form-control-file" name="prPdf">
                    </div>
                    <div class="form-group">
                        <label for="File">Product Image</label>
                        <input type="file" class="form-control-file" name="imgFile">
                    </div>
                    <div class="form-group">
                        <label for="cat">Select Category:</label>
                        <select class="form-control" id="cat" name="category">
                        <?php foreach($data as $key => $value)
                            { ?>
                            <option value="<?php echo $value['cy_id'] ?>"> <?php echo $value['cy_id']." : ".$value['cy_name']; ?> </option>
                            <?php } ?>
                        </select>
                    </div>
                     <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
<?php
include '../classProduct.php';
$insert= new Products($conn);
if(isset($_POST["submit"]))
    {   

        //Upload image
        $target_dir = "../imgs/";
        $imgDst = $target_dir . basename($_FILES["imgFile"]["name"]);
        move_uploaded_file($_FILES["imgFile"]["tmp_name"],$imgDst);
        //Upload pdf
        $fnm = "pdf/";
        $pdfDst = $fnm . basename($_FILES["prPdf"]["name"]);
        move_uploaded_file($_FILES["prPdf"]["tmp_name"],$pdfDst);
        
        $prFeat= $_POST['prFeat'];
        $prodName= $_POST['prName'];
        $desc= $_POST['prDesc'];    
        $category=$_POST['category'];
        $insert->addProducts($prodName,$desc,$imgDst,$prFeat,$pdfDst,$category);
        echo '<script language="javascript">';
        echo 'alert("Submitted");';
        echo '</script>';
    }
   
    
?>
