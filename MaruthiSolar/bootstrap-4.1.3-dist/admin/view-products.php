<?php
//session_start();
require_once '../classProduct.php';
require_once '../db_config.php';	
require_once 'classChecklogin.php';
  
$val = new checkLogin($conn);
$check = $val->loginCheck();
   
$obj = new Products($conn);
$data = $obj->getProducts();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin Panel</title>
	<meta name="description" content="Admin Content">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../css/base.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!--<script src="js/bootstrap.js"></script>
    <script src="js/jquery-3.4.1.js"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js">
	</script> <![endif]-->
</head>
<body>
<div class="container-fluid">
<h2><a href="adminPanel.php">Admin Panel</a></h2>
  <div class="row">
      <div class="col-md-3">
     
  <ul class="nav flex-column">
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="#">Products</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="addProducts.php">Add Products</a>
        </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="#">Categories</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="addCategories.php">Add Categories</a>
            <a class="dropdown-item" href="view-category.php">View Categories</a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="#">Form</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="view-form.php">View form</a>
        </div>
    </li>
    <li class="nav-item"><a href="logout.php">Logout</a></li>
  </ul>
  </div>
  <div class="col-md">
  <div class="table-responsive">
        <table class="table condensed">
            <thead>
                <tr>
                    <td>Product ID</td>
                    <td>Product name</td>
                    <td>Product description</td>
                    <td>Product image</td>
                    <td>Product features</td>
                    <td>Product pdf</td>
                    <td>Product Category</td>
                </tr>
            </thead>
            <tbody>
            <?php foreach($data as $key => $value)
                { ?>
                <tr>
                   <td><?php echo $value['pr_id']; ?></td>
                   <td><?php echo $value['pr_name']; ?></td>
                   <td><?php echo $value['pr_desc']; ?></td>
                   <td><?php echo $value['pr_img']; ?></td>
                   <td><?php echo $value['pr_feat']; ?></td>
                   <td><?php echo $value['pr_pdf']; ?></td>
                   <td><?php echo $value['pr_cat']; ?></td>
                   <td><a href="edit-product.php?pid=<?php echo $value['pr_id'] ?>&pnm=<?php echo $value['pr_name'] ?>&pdesc=<?php echo $value['pr_desc'] ?>&pfeat=<?php echo $value['pr_feat'] ?>&pcat=<?php echo $value['pr_cat'] ?>">Edit</a></td>
                   <td><a href="del-product.php?pid=<?php echo $value['pr_id'] ?>">Delete</a></td> 
                   
                <?php }?>       
                </tr>
            </tbody>
        </table>
    </div>
</div>
</div>

</body>