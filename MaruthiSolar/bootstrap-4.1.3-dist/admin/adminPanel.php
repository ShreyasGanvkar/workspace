<?php
  require_once '../db_config.php';
  require_once 'classChecklogin.php';
//  session_start();
 // print_r($_SESSION);
 $obj = new checkLogin($conn);
 $data = $obj->loginCheck();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin Panel</title>
	<meta name="description" content="Admin Content">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../css/base.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!--<script src="js/bootstrap.js"></script>
    <script src="js/jquery-3.4.1.js"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js">
	</script> <![endif]-->
</head>
<body>
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">
          <div class="card">  
            <div class="card-header">
              <h2>Dashboard</h2>
            </div>
            <div class="card-body">
              <ul class="nav flex-column">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="#">Products</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="addProducts.php">Add Products</a>
                        <a class="dropdown-item" href="view-products.php">View Products</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="#">Categories</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="addCategories.php">Add Categories</a>
                        <a class="dropdown-item" href="view-category.php">View Categories</a>
                    </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="#">Form</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="view-form.php">View form</a>
                    </div>
                </li>
                <li class="nav-item">
                  <a href="logout.php" class="pl-3">Logout</a>
                </li>
              </ul>
            </div> 
            </div>
        </div>  
      </div>
  </div>
</body>