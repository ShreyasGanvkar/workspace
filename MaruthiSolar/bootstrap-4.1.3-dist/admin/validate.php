<?php 
  require_once '../db_config.php'; 
  
  
  function test_input($data) { 
        
      $data = trim($data); 
      $data = stripslashes($data); 
      $data = htmlspecialchars($data); 
      return $data; 
  } 

  if ($_SERVER["REQUEST_METHOD"]== "POST") { 
  
      $adminname = test_input($_POST["uName"]); 
      $password = test_input($_POST["uPwd"]); 
      $sql = "SELECT * FROM admin";
	  $sql_stmt = $conn->prepare($sql);
      $sql_stmt->execute();
      $row = $sql_stmt->fetchAll(PDO::FETCH_ASSOC);
     
      foreach($row as $id) { 
            
          if(($id['username'] == $adminname) &&  
              ($id['password'] == $password)) {    
				session_start();
                  $_SESSION['logged_in']= $id['username'];
				
                  header("location: adminPanel.php");
					        exit;
          } 
          else { 
              header("location: index.php");
              echo '<script>';
              echo 'alert("Wrong information");';
              echo '</script>';
            } 
      } 
  } 
    
  ?> 
  