<?php
//session_start();
require_once '../db_config.php';
require_once 'classChecklogin.php';
  
$val = new checkLogin($conn);
$check = $val->loginCheck();
    
$cid=$_GET['cid'];
$cnm=$_GET['cnm'];
$cdesc=$_GET['cdesc'];

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin Panel</title>
	<meta name="description" content="Admin Content">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../css/base.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!--<script src="js/bootstrap.js"></script>
    <script src="js/jquery-3.4.1.js"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js">
	</script> <![endif]-->
</head>
<body>
<div class="container-fluid">
<h2><a href="adminPanel.php">Admin Panel</a></h2>
  <div class="row">
      <div class="col-md-3">
  <ul class="nav flex-column">
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="#">Products</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="addProducts.php">Add Products</a>
            <a class="dropdown-item" href="view-products.php">View Products</a>
        </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="#">Categories</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="addCategories.php">Add Categories</a>
            <a class="dropdown-item" href="view-category.php">View Categories</a> 
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="#">Form</a>
           <div class="dropdown-menu">
              <a class="dropdown-item" href="view-form.php">View form</a>
            </div>
    </li>
    <li class="nav-item"><a href="logout.php">Logout</a></li>
  </ul>
  </div>
  <div class="col-md">
                <form method="POST">
                    <div class="form-group">
                        <label for="categoryName">Category ID:</label>
                        <input type="text-box" class="form-control" value="<?php echo $cid ?>" name="cyId">
                    </div>
                    <div class="form-group">
                        <label for="categoryName">Category Name:</label>
                        <input type="text-box" class="form-control" value="<?php echo $cnm ?>" name="cyName">
                    </div>
                    <div class="form-group">
                        <label for="cyDesc">Category Description:</label>
                        <textarea class="form-control" rows="5" name="cyDesc"><?php echo $cdesc ?></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary" name='submit'>Submit</button>
                </form>
            </div>    
  </div>
</div>
  <?php 
  require_once '../classCategory.php';
    if(isset($_POST['submit']))
        {
            $catID=$_POST['cyId'];
            $catNm=$_POST['cyName'];
            $catDesc=$_POST['cyDesc'];
            $obj= new Category($conn);
            $insert=$obj->editCategories($catID,$catNm,$catDesc);
            echo '<script language="javascript">';
            echo 'alert("Submitted");';
            echo '</script>';
        }
  ?>
  </body>
  </html>
