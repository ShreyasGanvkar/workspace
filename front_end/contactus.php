<?php include('header.php')?>
<?php include('navbar.php')?>
<div class="container-fluid">
	<div class="row cover-contact-us">
		<div class="col-md w-100 text-center">
		<h1 class="mt-5">Contact Us</h1>
			<img src="images/contact-2.gif" class="img-fluid">
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row mt-5">
		<div class="col-md">
			<div class="row">
				<div class="col-md mt-5">
					<div class="row">
						<div class="col-md ">
							<address class="lead ms-5">
								<b class="footer-heading  fw-normal fs-5">Maruthi Solar Systems</b></br>
								41,42,43, Sy. No. 10/1</br>
								Abbigere Main Road</br>
								Kereguddadahalli</br>
								Bangalore - 560090
							</address>
						</div>
						<div class="col-md">
							<ul class="list-group lead">
								<li class="list-group-item border-0 fw-normal py-0 fs-5 footer-heading">Contact</li>
								<li class="list-group-item border-0 py-0">info@maruthisolar.com</li>
								<li class="list-group-item border-0 py-0">sales@maruthisolar.com</li>
								<li class="list-group-item border-0 py-0">+91-80-23253889</li>
								<li class="list-group-item border-0 py-0">+91 9980140804</li>
							</ul>	
						</div>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-md">
				  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15545.628881780007!2d77.51453012782113!3d13.07335845262569!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae3d2870000001%3A0xce27106b09962baf!2mdaruthi%20Solar%20Systems!5e0!3m2!1sen!2sin!4v1585739829978!5m2!1sen!2sin" width="100%" height="100%" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> 	    
				</div>
			</div>
		</div>
		<div class="col-md">
			<div class="jumbotron">
                 <h3 class="text-center mt-5 mb-3 footer-heading">How can we help you?</h3>
                <form action="" method="post" class="card p-3">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" name="nme" required>
                    </div>
                    <div class="form-group">
                        <label for="dsg">Designation:</label>
                        <input type="text" class="form-control" id="dsg" name="dsg" required>
                    </div>
                    <div class="form-group">
                        <label for="org">Organization:</label>
                        <input type="text" class="form-control" id="org" name="org" required>
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail:</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="city">City:</label>
                        <input type="text" class="form-control" id="city" name="city" required>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone no.:</label>
                        <input type="tel" pattern="[0-9]{10}" placeholder="1234567890" class="form-control" id="phno" name="phno" required>
                    </div>
                    <div class="form-group">
                        <label for="requirements">Requirements:</label>
                        <input type="text" class="form-control" id="req" name="req" required>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3 " name="submit"  >Submit</button>
                    <!--button type="reset" class="btn btn-primary mt-3 ms-3">Clear</button-->
                </form>
            </div>  
		</div>
	</div>
</div>	
<?php include('footer-bar.php')?>
<?php include('footer.php')?>