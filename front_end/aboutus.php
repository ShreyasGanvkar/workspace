<?php include('header.php')?>
<?php include('navbar.php')?>
<div class="container-fluid">
	<div class="row about-us-cover">
		<div class="col-md w-100 text-center">
			<h1 class="mt-5">About Us</h1>
		</div>
	</div>
	<div class="container">
		<div class="row mt-5">
			<div class="col-md">
				<h1 class="Display 1">Maruthi Solar is Your Partner In Power</h1>
				<h3 class="text-muted">
					that has been evaluated as ' A' Grade supplier based on our performance for the last many years by our customers like BHEL, BEL, TATA BP Solar & Reliance Industries.
				</h3>
			</div>
			<div class="col-md">
				<p class="fw-normal lead">
					The company is managed by technically qualified, professionally experienced, Young and Dynamic Technocrats. The in- house Research and Development, helps in developing the products of high quality and reliability with optimum design and considerable reduction in cost. The quality being the most important parameter and with guaranteed after sales service of our products have helped us to grow to a considerable level. The Maruthi Solar Systems is a company with fifty lakhs of rupee turn-over, has carried out the major works related solar Photovoltaic systems.
				</p>
			</div>
		</div>
	</div>
</div>

<!--div class="container">
	<div class="row">
		<div class="col-md text-center my-5">
			<h1 class="mt-3">Our Projects</h1>
			<p class="fw-normal lead">Maruthi Solar Systems is proud to offer various power solutions to suit your needs</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md">
			<img class="img-fluid" src="images/solar_street_lighting_syste.jpg">
		</div>
		<div class="col-md">
			<p class="lead fw-normal">
			Designed, Manufactured, Installed and commissioned more than five hundred numbers of solar Powered Street Lighting systems in rural part of the Karnataka State. Designed and developed Solar Powered Lantern as per Ministry of Non- Conventional Energy Sources New-Delhi specifications and supplied to M/s Bharat Electronics, Bangalore of around ten thousand numbers.
			</p>
			<p class="lead fw-normal">
			Designed and developed Solar Powered Street Lighting systems as per Ministry of Non- conventional Energy Sources, New Delhi specifications and supplied to M/s Bharat Electronics, Bangalore and Bharat Heavy Electricals Limited, Bangalore of more than three thousand numbers.
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md">
			<p class="lead fw-normal">
			Designed, developed, Manufactured & supplied solar Powered Traffic Blinkers to M/s Bharat Heavy Electrical limited, Bangalore.
			</p>
			<p class="lead fw-normal">
			Designed and developed Solar/ mains hybrid power supplies for fixed wireless terminals as per Department of Telecommunication specifications and supplied to DOT suppliers.
			</p>
			<p class="lead fw-normal">
			Designed, developed and supplied Solar/ mains hybrid power plant to Karnataka Police Department around 300 nos. for computer application.
			</p>
		</div>
		<div class="col-md">
			<img class="img-fluid" src="images/power-plant-1.jpg">
		</div>
	</div>
	<div class="row">
		<div class="col-md">
			<img class="img-fluid" src="images/power-plant-1.jpg">
		</div>
		<div class="col-md">
			<p class="lead fw-normal">
			We are proud to convey that we are OE Manufactures for M/s Bharat Electronics, Bangalore, Bharat Heavy Electrical Limited, Bangalore and major Solar PV manufacturing Companies in Bangalore. Apart from the above Non-conventional power products, more than 2000 numbers of OFF-LINE UPS systems of various capacities ranging from 250 VA to 2 KVA have been manufactured and installed in and around Bangalore and other districts in Karnataka.
			</p>
			<p class="lead fw-normal">
			Our company has been evaluated as ‘A’ grade supplier based on our performance for the last four to five years by our customers like BHEL, BEL, TATA BP SOLAR and SHELL SOLAR
			</p>
		</div>
	</div>
</div-->		

<div class="container-fluid">
	<div class="row cover mt-5">
		<div class="col-md-7">
			<h1 class="my-5">About Our Organisation</h1>
			<p class="fw-normal lead fs-3 pe-5">
 			The company has a core team with eight qualified technical staff at various levels and with support for various ancillary industries for assembly, wiring, supply of raw materials, fabrication of sheet metal works, etc. The man power is upgraded from time to time, to provide uninterrupted services to our clients.
			</p>
		</div>
		<div class="col-md-5">
			<img src="images/circle.png" class="img-fluid w-100" >
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row cover mt-5">
		<div class="col-md-5">
			<img src="images/circle.png" class="img-fluid w-100" >
		</div>
		<div class="col-md-7">
			<h1 class="my-5">Celebrating 20+ years!</h1>
			<p class="fw-normal lead fs-3 pe-5">
 			Maruthi Solar Systems has achieved continuous respect from the solar industry for more than two decades of experience as a leading provider of reliable, innovative energy solutions.Maruthi has been evaluated as ' A' Grade supplier based on our performance for the last many years by our customers like BHEL, BEL, TATA BP Solar & Reliance Industries.
			</p>
		</div>
	</div>
</div>
<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center mt-5" style="color:#529BFF">Get In Touch With Us</h2>
				<button class="btn btn-lg d-grid mx-auto my-5"  type="button" style="color:#FFFFFF;background-color:#529BFF">CONTACT US</button>
			</div>
		</div>
</div>
<?php include('footer-bar.php') ?>
<?php include('footer.php') ?>