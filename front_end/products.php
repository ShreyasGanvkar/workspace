<?php include('header.php')?>
<?php include('navbar.php')?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md w-100 text-center">
			<img src="images/leads.gif" class="img-fluid">
			<h1 class="mt-5">Products</h1>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-4" style="border 2px solid">
			<h6>Filter by Category</h6>
			<nav class="navbar">
				<ul class="navbar-nav" id="category-list">
					<li class="nav-item">
						<a class="nav-link" href="#">All Products</a>
					</li> 		
				</ul>
			</nav>
		</div>
		<div class="col-md-8" style="border 2px solid">
		
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center mt-5" style="color:#529BFF">Get In Touch With Us</h2>
			<button class="btn btn-lg d-grid mx-auto my-5"  type="button" style="color:#FFFFFF;background-color:#529BFF">CONTACT US</button>
		</div>
	</div>
</div>
<?php include('footer-bar.php') ?>
<?php include('footer.php') ?>