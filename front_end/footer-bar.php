<div class="container-fluid">
	<div class="row border-top">
		<div class="col-md-3">
			<img class="my-5 ms-4" src="images/logo.png" alt="">
		</div>
		<div class="col-md-2">
			<ul class="list-group mt-5">
				<li class="list-group-item border-0 lead fw-normal pt-0 fs-5 footer-heading"><b>Explore</b></li>
				<li class="list-group-item border-0 lead"><a class="link-dark footer-link" href="home.php">Home</a></li>
				<li class="list-group-item border-0 lead pt-0"><a class="link-dark footer-link" href="aboutus.php">About us</a></li>
				<li class="list-group-item border-0 lead pt-0"><a class="link-dark footer-link" href="products.php">Products</a></li>
				<li class="list-group-item border-0 lead pt-0"><a class="link-dark footer-link" href="contactus.php">Contact us</a></li>
			</ul>
		</div>
		<div class="col-md-3">
			<ul class="list-group mt-5">
				<li class="list-group-item border-0 lead fw-normal pt-0 fs-5 footer-heading"><b>Visit</b></li>
				<li class="list-group-item border-0 lead">Maruthi Solar Systems</li>
				<li class="list-group-item border-0 lead pt-0"># 41,42,43, Sy. No. 10/1</li>
				<li class="list-group-item border-0 lead pt-0">Abbigere Main Road</li>
				<li class="list-group-item border-0 lead pt-0">Kereguddadahalli</li>
				<li class="list-group-item border-0 lead pt-0">Bangalore - 560090</li>
			</ul>
		</div>
		<div class="col-md-2">
			<ul class="list-group mt-5">
				<li class="list-group-item border-0 lead fw-normal pt-0 fs-5 footer-heading"><b>Contact</b></li>
				<li class="list-group-item border-0 lead">info@maruthisolar.com</li>
				<li class="list-group-item border-0 lead pt-0">sales@maruthisolar.com</li>
				<li class="list-group-item border-0 lead pt-0">+91-80-23253889</li>
				<li class="list-group-item border-0 lead pt-0">+91 9980140804</li>
			</ul>
		</div>
		<div class="col-md-2">
			<ul class="list-group mt-5">
				<li class="list-group-item border-0 lead fw-normal pt-0 fs-5 footer-heading"><b>Follow</b></li>
				<li class="list-group-item border-0 lead">Facebook</li>
				<li class="list-group-item border-0 lead pt-0">Instagram</li>
			</ul>	
		</div>
	</div>
</div>