<div class="container-fluid">
	<div class="row cover">
		<div class="col-md w-100 text-center">
			<img class="mt-5 img-fluid" src="images/sun.gif">
			<h1 class="mb-3 ml3">Welcome to Maruthi Solar Systems</h1>
			<button type="button" class="btn btn-outline-info btn-lg my-5">VIEW OUR PRODUCTS</button>
		</div>
	</div>
</div>	
	<div class="container">
		<div class="row">
			<div class="col-md mt-2 text-center">
				<h1 class="mt-5 ml3">How Can We Serve You?</h1>
				<p class="mt-5 fw-normal lead">
					Maruthi Solar Systems Bangalore, an ISO 9001-2008 certified, proprietary concern company was started in the year 1998.</br> The company strength lies in system design, product development, sourcing, manufacturing, installation & commissioning of solar photovoltaic Systems.
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="card shadow my-5 text-center">
				  <img src="images/eye2.gif" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Our Vision</h5>
						<p class="card-text fw-normal lead mt-3">
							To be a worldwide solar product Provider to meet the global energy needs.
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card shadow my-5 text-center">
				<img src="images/Su4d.gif" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Our Services</h5>
						<p class="card-text fw-normal lead mt-3">
							We have successfully designed, developed, manufactured & supplied more than 2.5million solar systems world wide.
						</p>
					</div>
				</div>
			</div>	
			<div class="col-md-4">
				<div class="card shadow my-5 text-center">
				<img src="images/rocket.gif" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Our Mission</h5>
						<p class="card-text fw-normal lead mt-3">
							To Provide cost effective & reliable solar energy solutions.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row cover mt-5">
			<div class="col-md-5">
				<img src="images/circle.png" class="img-fluid w-100" >
			</div>
			<div class="col-md-7">
				<h1 class="my-5">Celebrating 20+ years!</h1>
				<p class="fw-normal lead fs-3 pe-5">
					Maruthi Solar Systems has achieved continuous respect from the solar industry for more than two decades of experience as a leading provider of reliable, innovative energy solutions.Maruthi has been evaluated as ' A' Grade supplier based on our performance for the last many years by our customers like BHEL, BEL, TATA BP Solar & Reliance Industries.
				</p>
			</div>
		</div>
	</div>
	<!--div class="container-fluid">
		<div class="row">
			<div class="col-md color-block-1 text-center">
				<h1>Client Testimonials</h1>
				<div class="row">
					<div class="col-md ms-5 me-3 embed-responsive embed-responsive-21by9 video-box">
						<iframe class="embed-responsive-item w-100 h-100" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen"></iframe>
					</div>
					<div class="col-md me-5 ms-3 embed-responsive embed-responsive-21by9 video-box">
						<iframe class="embed-responsive-item w-100 h-100" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen"></iframe>
					</div>
				</div>
			</div>
		</div>
		<div class="row .color-block-2">
			<div class="col-md">
				
			</div>
		</div>
	</div-->
	<div class="container">
		<div class="row">
			<div class="col-md text-center my-5">
				<h1 class="mt-3">Our Products</h1>
				<p class="fw-normal lead">Maruthi Solar Systems is proud to offer various power solutions to suit your needs</p>
			</div>
		</div>
		<div class="row card-group mb-5">
			<div class="card border-start-0 border-top-0 border-bottom-0">
				<img src="images/01.jpg" class="card-img-top" alt="...">
				<div class="card-body">
					<h5 class="card-title fs-4">Solar Charge Controller</h5>
					<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
				</div>
			</div>
			<div class="card border-top-0 border-bottom-0">
				<img src="images/03.jpg" class="card-img-top" alt="...">
				<div class="card-body">
					<h5 class="card-title fs-4">Solar Charge Controller</h5>
					<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
				</div>
			</div>
			<div class="card border-top-0 border-bottom-0">
				<img src="images/28.jpg" class="card-img-top" alt="...">
				<div class="card-body">
					<h5 class="card-title fs-4">Card title</h5>
					<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
				</div>
			</div>
			<div class="card border-end-0 border-top-0 border-bottom-0">
				<img src="images/101.jpg" class="card-img-top" alt="...">
				<div class="card-body">
					<h5 class="card-title fs-4">Card title</h5>
					<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
				</div>
			</div>
		</div>
		<button class="btn btn-lg d-grid mx-auto my-5 btn-view" type="button" >VIEW OUR PRODUCTS</button>
	</div>
	<div class="container-fluid cover">
		<!--div class="row  mx-5">
			<div class="col-md mt-5 text-center window">
				<h2 class="my-3">When Energy Is Essential,</h2>
				<h1>Maruthi Solar Is Your Partner In Power</h1>
			</div>
		</div-->
		<div class="row  mx-5">
			<div class="col-md mt-5 text-center">
				<h2 class="my-3">When Energy Is Essential,</h2>
				<h1>Maruthi Solar Is Your Partner In Power</h1>
			</div>
		</div>
	<div class="container">	
		<div class="row mt-5">
			<div class="col-md border-end border-bottom">
				<div class="row my-5">
					<div class="col-md-4">
						<img src="images/bulb.gif" class="thumbnail">
					</div>
					<div class="col-md-8 pt-4">
						<h4>Innovative</h4>
						<p>Leader in advanced technologies for clean, reliable and affordable energy.</p>
					</div>
				</div>
			</div>
			<div class="col-md border-start border-bottom">
				<div class="row my-5">
					<div class="col-md-4">
						<img src="images/rupee.gif" class="thumbnail">
					</div>
					<div class="col-md-8 pt-4">
						<h4>Cost-Effective</h4>
						<p>Optimizing the cost-effectiveness ratio to minimize the total cost of ownership.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md border-top border-end">
				<div class="row my-5">
					<div class="col-md-4">
						<img src="images/handshake.gif" class="thumbnail">
					</div>
					<div class="col-md-8 pt-4">
						<h4>Reliable</h4>
						<p>Setting industry standards for top quality and superior reliability.</p>
					</div>
				</div>
			</div>
			<div class="col-md border-top border-start">
				<div class="row my-5">
					<div class="col-md-4">
						<img src="images/flexibility.gif" class="thumbnail">
					</div>
					<div class="col-md-8 pt-4">
						<h4>Flexible</h4>
						<p>Providing power goods with versatility to create maximum off-grid, on-grid installations.</p>
					</div>
				</div>
			</div>
			<button class="btn btn-lg d-grid mx-auto mt-2 mb-5 btn-view" type="button" >VIEW ABOUT US</button>
		</div>
		
	</div>
	</div>
	<!--div class="container-fluid">
		<div class="row">
			<div class="col-md-12 cover">
				<h2 class="text-center mt-5">Get In Touch With Us</h2>
				<button class="btn btn-lg d-grid mx-auto my-5 btn-contact" type="button">CONTACT US</button>
			</div>
		</div>
	</div-->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center mt-5" style="color:#529BFF">Get In Touch With Us</h2>
				<button class="btn btn-lg d-grid mx-auto my-5"  type="button" style="color:#FFFFFF;background-color:#529BFF">CONTACT US</button>
			</div>
		</div>
</div>
<script>

var textWrapper = document.querySelector('.ml3');
textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

anime.timeline()
  .add({
    targets: '.ml3 .letter',
    opacity: [0,1],
    easing: "easeInOutQuad",
    duration: 2250,
    delay: (el, i) => 150 * (i+1)
  });
</script>

