<!DOCTYPE HTML>
<?php error_reporting(0);?>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Admin panel</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" >
	</head>
<body>
	<div class="header">
		<div class="logo">
			Admin Panel
		</div>
	</div>
	<div class="container">
		<div class="sidebar">
			<ul class="navbar">
				<li class="dropdown">
					<a href="">Items</a>
						<ul class="dropdown-content">
							<li><a href="additems.php">Add items</a></li>
							<li><a href="">Delete items</a></li>
							<li><a href="">Update Items</a></li>
						</ul>
				</li>
				<li class="dropdown">
					<a href="">Shops</a>
						<ul class="dropdown-content">
							<li><a href="addshop.php">Add shop</a></li>
							<li><a href="">Delete shop</a></li>
							<li><a href="">Update shop</a></li>
						</ul>
				</li>
				<li class="dropdown">
					<a href="">Delivery</a>
						<ul class="dropdown-content">
							<li><a href="addprofile.php">Add profile</a></li>
							<li><a href="">Delete profile</a></li>
							<li><a href="">Update profile</a></li>
						</ul>
				</li>
			</ul>
		</div>
		
		<div class="content-box">
			<div class="content">
			<form action="" method="post" enctype="multipart/form-data">
				<h2>Add Items</h2>
				<div class="item">
					<input type="file" name="file" />
				</div>
				<div class="item">
					<label >Item name</label>
					<input type="text" name="itmname" placeholder="Enter item name"   />
				</div>
				<div class="item">
					<label >Description</label>
					<textarea name="itmdesc" rows='4' cols='12' placeholder="Enter item description" ></textarea>
				</div>
				<div class="item">
					<label>Categories</label>
					<select name="category">
						<option value="fg">Food grains</option>
						<option value="frvg">Fruits & Vegetables</option>
						<option value="Olms">Oils & Masalas</option>
						<option value="cghg">Cleaning & Hygeine</option>
					</select>	
				</div>
				<div class="item">
					<label>Shops</label>
					<select name="shop">
						<option value="narshiv">Narshiv general stores</option>
						<option value="sanjeev">Sanjeev shetty shop</option>
						<option value="mukund">Mukund naik</option>
						<option value="tamburi">Tamburi</option>
					</select>
				</div>
				<div class="item">
					<label>Price</label>
					<input type="text" name="price" placeholder="Enter the price" />
				</div>
				<div class="item">
					<input type="submit" name="submit" value="Add"/>
					<input style="margin-left:60px;" type="reset" name="reset" value="Reset" />
				</div>
				</form>
			</div>
		</div>
	
	</div>
</body>
</html>
<?php
error_reporting(0);
$link = mysqli_connect("localhost", "root", "", "e_vyapari");
if(isset($_POST["submit"])){
$fnm=$_FILES["file"]["name"];
$dst="./product_image/".$fnm;
move_uploaded_file($_FILES["file"]["tmp_name"],$dst);    
$itemname= mysqli_real_escape_string($link, $_REQUEST['itmname']);
$description = mysqli_real_escape_string($link, $_REQUEST['itmdesc']);
$price = mysqli_real_escape_string($link, $_REQUEST['price']);
$category=$_REQUEST["category"];
$sql = "INSERT INTO item (iname, rate ,description, picture, catname) VALUES ('$itemname', '$price', '$description', '$fnm', '$category')";
if(mysqli_query($link, $sql)){
    echo "Records added successfully";
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}
 
}
mysqli_close($link);
?>