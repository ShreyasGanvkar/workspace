<?php include('header.php') ?>
<h2 align='center'>About Us</h2>
	<p>The �E-Vyapari� e-commerce website has been developed to make it easier for the user to get the necessary grocery items delivered to his doorstep. This online portal has also been developed for existing grocery shops in order to increase their sales and connect the customer and the shops. 
		
	This project requires very little knowledge in order to make purchases. No formal knowledge is required for the user to use this portal. This system hence developed is user-friendly, error free, secure and a reliable system. It will help shop owners to increase their sales by a profitable margin and also provide an easier way for a customer to make purchases.

		One of the advantages of ecommerce is that online stores are always open for business. By being available at all hours, you can attract people who would normally pick up a product in stores, if the store were open. You can also attract those who may have odd work schedules or who don�t have time to shop in-person. Hence, this system will allow the customer to fulfill his grocery requirements and at the same time helping the shop to generate income.<p>
<?php include('footer.php'); ?>		
