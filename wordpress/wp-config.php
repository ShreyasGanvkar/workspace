<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp' );

/** MySQL database username */
define( 'DB_USER', 'homestead' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Qwerty1234' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'tj5w&^9,|}2/`fb;*G9uLIrXEvU;o^4#1q>I-ZS6NtNy9Q|gH]LpGCPfej9V-Tg:' );
define( 'SECURE_AUTH_KEY',  'Po_l-E2B$b!c2UhkqF:4<}m4_h(fF{_dP~)S- ]Orc}*PU_w17s[eAjIe+]!8T%;' );
define( 'LOGGED_IN_KEY',    '~sX`~&u2s)ti@:]t1BMvz,VJ&7~G=:A79y:f=gOnyPBNLGZz79@ WF*-J?LYw&Q-' );
define( 'NONCE_KEY',        '66$2(Gv0w (pv.4eL9IV2lO# ,!K }&C,{;2n7JpIgTI.e> .>, R%Q[1HlDL9Qz' );
define( 'AUTH_SALT',        'd_!^*&|fIqzIGo/5kUyt(3f&`)`aM6Ci]n0i( }BqQr:1Au>_FH&R&mg>TP_j.4H' );
define( 'SECURE_AUTH_SALT', '4D5MqF-xQ3;iSw5Ig+4@;LCl0M.X;*V@P$_o<lirYd)z9g_>dW0eHULZ-/0pb{*w' );
define( 'LOGGED_IN_SALT',   '3-+iCS:?|!<wQ!K(wt x|wg(b1ID55-Ht>o%G|z}~_6<z^FK9>uUetQpBDAlqm`&' );
define( 'NONCE_SALT',       'D9j2>hhdL<AO}uU~2v`e~-I_wS2BbY?}<o#~CDjQ%~b[%CL][qk }Gm]2n6BuSS/' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
