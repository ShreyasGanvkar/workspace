-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 03, 2020 at 02:15 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trp`
--

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `clientID` int(11) NOT NULL AUTO_INCREMENT,
  `clientName` text NOT NULL,
  `industryID` int(11) NOT NULL,
  `state` text NOT NULL,
  `location` varchar(100) NOT NULL,
  PRIMARY KEY (`clientID`),
  KEY `foreign` (`industryID`)
) ENGINE=MyISAM AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`clientID`, `clientName`, `industryID`, `state`, `location`) VALUES
(1, 'Leading Private Sector Bank', 1, 'Andhra Pradesh', 'Guntur & Tirupati'),
(2, 'Leading Private Sector Bank', 1, 'Andhra Pradesh', 'Hyderabad'),
(3, 'Leading Private Sector Bank', 1, 'Andhra Pradesh', 'Hyderabad,Vijayawada'),
(4, 'Leading Private Sector Bank', 1, 'Andhra Pradesh', 'Hyderabad,Vijayawada,Nizamabad,Eluru & Khammam'),
(5, 'Leading Private Sector Bank', 1, 'Maharashtra', 'Mumbai'),
(6, 'Leading Private Sector Bank', 1, 'Maharashtra', 'Mumbai'),
(7, 'Leading Private Sector Bank', 1, 'Maharashtra', 'Mumbai'),
(8, 'Leading NBFC', 2, 'Uttar Pradesh', 'Lucknow'),
(9, 'Leading NBFC', 2, 'Andhra Pradesh', 'Hyderabad'),
(10, 'Leading NBFC', 2, 'Tamil Nadu', 'Chennai'),
(11, 'Leading NBFC', 2, 'Maharashtra', 'Mumbai'),
(12, 'Leading NBFC', 2, 'Maharashtra', 'Mumbai'),
(13, 'Leading NBFC', 2, 'Maharashtra', 'Mumbai'),
(14, 'Leading NBFC', 2, 'Maharashtra', 'Mumbai'),
(15, 'Leading NBFC', 2, 'Maharashtra', 'Mumbai'),
(16, 'Leading NBFC', 2, 'Maharashtra', 'Mumbai'),
(17, 'Leading NBFC', 2, 'Maharashtra', 'Mumbai & Pune'),
(18, 'Leading Pharmaceutical Company', 3, 'Madhya Pradesh', 'Ujjain'),
(19, 'Leading Pharmaceutical Company', 3, 'Bihar', 'Daulataganj'),
(20, 'Leading Pharmaceutical Company', 3, 'Chhattisgarh', 'Raigarh'),
(21, 'Leading Pharmaceutical Company', 3, 'Delhi', 'Delhi'),
(22, 'Leading Pharmaceutical Company', 3, 'Delhi', 'Delhi'),
(23, 'Leading Pharmaceutical Company', 3, 'Madhya Pradesh', 'Indore'),
(24, 'Leading Pharmaceutical Company', 3, 'Rajasthan', 'Bikaner'),
(25, 'Leading Pharmaceutical Company', 3, 'Rajasthan', 'Kota'),
(26, 'Leading Pharmaceutical Company', 3, 'Rajasthan', 'Kota'),
(27, 'Leading Pharmaceutical Company', 3, 'Uttar Pradesh', 'Ghaziabad'),
(31, 'Leading Pharmaceutical Company', 3, 'Andhra Pradesh', 'Kakinada'),
(30, 'Leading Pharmaceutical Company', 3, 'Uttar Pradesh', 'Lucknow'),
(32, 'Leading Pharmaceutical Company', 3, 'Karnataka', 'Bagalkot'),
(33, 'Leading Pharmaceutical Company', 3, 'Karnataka', 'Bangalore'),
(34, 'Leading Pharmaceutical Company', 3, 'Karnataka', 'Davangere'),
(35, 'Leading Pharmaceutical Company', 3, 'Karnataka', 'Hubli'),
(36, 'Leading Pharmaceutical Company', 3, 'Kerala', 'Cochin'),
(37, 'Leading Pharmaceutical Company', 3, 'Kerala', 'Quilon'),
(38, 'Leading Pharmaceutical Company', 3, 'Kerala', 'Tiruvella'),
(39, 'Leading Pharmaceutical Company', 3, 'Kerala', 'Trivandrum'),
(40, 'Leading Pharmaceutical Company', 3, 'Puducherry', 'Puducherry'),
(41, 'Leading Pharmaceutical Company', 3, 'Tamil Nadu', 'Chennai'),
(42, 'Leading Pharmaceutical Company', 3, 'Tamil Nadu', 'Chennai'),
(43, 'Leading Pharmaceutical Company', 3, 'Tamil Nadu', 'Coimbatore'),
(44, 'Leading Pharmaceutical Company', 3, 'Tamil Nadu', 'Cuddalore'),
(45, 'Leading Pharmaceutical Company', 3, 'Tamil Nadu', 'Dharampuri'),
(46, 'Leading Pharmaceutical Company', 3, 'Tamil Nadu', 'Madurai'),
(47, 'Leading Pharmaceutical Company', 3, 'Tamil Nadu', 'Nagercoil'),
(48, 'Leading Pharmaceutical Company', 3, 'Tamil Nadu', 'Salem'),
(49, 'Leading Pharmaceutical Company', 3, 'Tamil Nadu', 'Salem'),
(50, 'Leading Pharmaceutical Company', 3, 'Tamil Nadu', 'Trichy'),
(51, 'Leading Pharmaceutical Company', 3, 'Goa', 'Goa'),
(52, 'Leading Pharmaceutical Company', 3, 'Goa', 'Goa - South'),
(53, 'Leading Pharmaceutical Company', 3, 'Gujarat', 'Baroda'),
(54, 'Leading Pharmaceutical Company', 3, 'Gujarat', 'Surat'),
(55, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Akola'),
(56, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Aurangabad'),
(57, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Chandrapur'),
(58, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Mumbai - South'),
(59, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Mumbai - Thane'),
(60, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Mumbai - Thane'),
(61, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Mumbai - Thane'),
(62, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Mumbai - Vashi'),
(63, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Mumbai - Vashi'),
(64, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Mumbai - Western Suburbs'),
(65, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Nagpur'),
(66, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Nagpur'),
(67, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Pune'),
(68, 'Leading Pharmaceutical Company', 3, 'Maharashtra', 'Pune');

-- --------------------------------------------------------

--
-- Table structure for table `industry`
--

DROP TABLE IF EXISTS `industry`;
CREATE TABLE IF NOT EXISTS `industry` (
  `industryID` int(11) NOT NULL AUTO_INCREMENT,
  `industryName` text NOT NULL,
  PRIMARY KEY (`industryID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industry`
--

INSERT INTO `industry` (`industryID`, `industryName`) VALUES
(1, 'Bank'),
(2, 'NBFC'),
(3, 'Pharmaceutical');

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

DROP TABLE IF EXISTS `info`;
CREATE TABLE IF NOT EXISTS `info` (
  `infoID` int(11) NOT NULL AUTO_INCREMENT,
  `jobID` int(11) DEFAULT NULL,
  `can_name` tinytext DEFAULT NULL,
  `can_email` varchar(50) DEFAULT NULL,
  `can_phone` varchar(10) DEFAULT NULL,
  `can_radio` tinytext DEFAULT NULL,
  `can_cv` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`infoID`),
  KEY `jobID` (`jobID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`infoID`, `jobID`, `can_name`, `can_email`, `can_phone`, `can_radio`, `can_cv`) VALUES
(1, 1, 'asasas', 'adad@dsd', '7899149177', 'm', 'cv/hrdb.png'),
(2, 20, 'Shreyas', 'shreyas@gmail.com', '7899149177', 'm', 'cv/ER_notations.png');

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
CREATE TABLE IF NOT EXISTS `job` (
  `jobID` int(11) NOT NULL AUTO_INCREMENT,
  `clientID` int(11) NOT NULL,
  `position` text NOT NULL,
  `qualification` text NOT NULL,
  `deliverables` varchar(30000) NOT NULL,
  `skills` varchar(2000) NOT NULL,
  `experience` varchar(500) NOT NULL,
  `compensation` text NOT NULL,
  PRIMARY KEY (`jobID`),
  KEY `foreign` (`clientID`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`jobID`, `clientID`, `position`, `qualification`, `deliverables`, `skills`, `experience`, `compensation`) VALUES
(1, 1, 'Branch Manager', 'Post Graduate', '1) To Maintain Profit and loss account of Branch. 2) To Ensure all Resources in the Branch are optimally utilized. 3) To Ensure all RM in the Branch meet monthly acquisition of CASA accounts. 4) To Ensure Wealth RM meet their monthly revenue targets.', '1) Ideal candidate should be an branch Manager currently at a Branch. 2) current branch Managers also can apply for the role. 3) Candidate should have minimum 5 to 6 work experience with acquisition of Retail liabilities.', '5 - 6 years', 'The Best in the industry.'),
(2, 2, 'Relationship Manager – Transaction Banking Group', 'Post Graduate', '1. To acquire transaction intensive clients using Lending products as a hook and generate fees revenue with threshold of Rs 4 lac per client in a year thru trade/forex/CMS/Lending products. 2. Responsible for achieving topline and revenue targets as per design across assets ANR, of clients meeting minimum CASA threshold, # of Trade and Fx clients, revenue from trade and FX clients, Business lending PF, Business lending ANR. 3. Achieve the targets through New client acquisition Cross sell ratio of Trade, FX , Business lending, CMS and payment products. 4. Execution of Client Coverage model for new to bank acquisition Implement and execute the sales process as per design for TBG . Reports to Regional Head – TBG .', 'Good Communication Skills, Relationship Management.', 'Minimum 5 - 10 years of experience.', 'The Best in the industry.'),
(3, 3, 'Associate Relationship Manager – Transaction Banking Group', 'Graduate or Post Graduate', '1. To acquire transaction intensive clients using Lending products as a hook and generate fees revenue with threshold of Rs 4 lac per client in a year thru trade/forex/CMS/Lending products. 2. Responsible for achieving topline and revenue targets as per design across assets ANR, of clients meeting minimum CASA threshold, # of Trade and Fx clients, revenue from trade and FX clients, Business lending PF, Business lending ANR. 3. Achieve the targets through New client acquisition Cross sell ratio of Trade, FX , Business lending, CMS and payment products. 4. Execution of Client Coverage model for new to bank acquisition 5. Implement and execute the sales process as per design for TBG', 'Good Communication Skills, Relationship Management', '2 - 6 years of experience', 'The Best in the industry.'),
(4, 4, 'Relationship Managers – Investment & Advisory Services ( Grade - Sr. Officer / Manager )', 'MBA\'s preferred', '* Selling of Life Insurance,Gold Coins,GI & Mutual Funds. * To Ensure Wealth RM meet their monthly revenue targets across all above products. * Experience in selling wealth products/TPP products.', '• Should have good financial & analytical skills. • To establish relationship with competitors to have updated information about the changes happening in the industry. • Collaborative & Co-ordinations Skills • Aggressive approach. • Analytical Skills • Sound problem solving and decision making skills • Good communications and interpersonal skills • Computer literate in MS Office • Proficient in both spoken and written English', 'Minimum 2 - 9 years of experience ( Current position in Banking Industry).', 'The Best in the industry.'),
(5, 5, 'Policy Manager', 'CA, MBA Finance', 'Function - Chief Credit Officer Business Group - Retail Credit & Policy Products - Credit Cards & Personal Loans For setting up: • Policy and processes for credit cards & PL • Portfolio monitoring mechanism & other internal controls for the respective products • Policy formulation & placing to various committees for approval • Defining process flow and identifying risks & controls • Policy implementation through system • Portfolio monitoring, trigger review & delinquency control • Collaboration with functional groups like Product, Sales, Audit, Inspection & Vigilance, IT, etc. • Understanding of regulatory aspects w.r.t. product • Training of teams Position reporting to - Head – Retail Policy & Risk', '* Analytical Skills * Document drafting, presentation & excel computation skills * Good process orientation', '4 - 5 years out of which 3 - 4 years of relevant experience', 'The Best in the industry.'),
(6, 6, 'Product Head - Mortgage', 'C.A / CFA / MBA / CAIIB / ICWA', '• Defining the target customer segment & markets for the Mortgages business (HL,LAP & CF) • Designing new products /modifying existing product based on market/potential customer feedback, competition analysis, sales inputs • Creating new avenues for sourcing of business – distribution • Relationship management with key stake holders – customers, channels, service providers • P&L projections and achieving the top-line& bottom line numbers • Cost management • Product pricing & distribution channel structure management • Process management for end to end product life cycle • Cross sell initiatives for income generation/ securing portfolio • New products/ new customer segments to be targeted – upsell • Driving sales by working closely with the sales team, distribution channels, launching schemes/contests • Visibility campaigns – ATL/BTL and working closely with the branding team for ensuring brand recall and brand identity communication • MIS & business analysis • Portfolio quality analysis & taking necessary control actions', '• Interpersonal Skills • Result Oriented • Collaborative & Co-ordinations Skills • Sound problem solving and decision making skills • Good communications and interpersonal skills', '9-11 yrs', 'The Best in the industry.'),
(7, 7, 'Area / Regional Manager - SME', 'MBA / CA', '• Lead and manage a team of RM/ SRM across portfolio and acquisition (HUNTER) role. • Responsible for sales management of sales team and assigned market. • Ensure a stellar customer experience by driving teams to meet and exceed customer expectations through effective relationship management, ensuring customer banking needs are being met, and fully representing the Bank. • Protect the bank’s assets and reputation by knowing the customer and their business, monitoring for suspicious behaviour, and taking appropriate action as set forth in operating policies when necessary. Impact on the Business: • Develop and implement plans to manage portfolios of SME business customers within assigned local market of varying complexity and nature, focusing on increasing product penetration, profitability, and customer satisfaction. Create and implement a targeted plan to contact prospects in sufficient quantity to achieve financial objectives. • Leverage Bank’s brand, capabilities, and existing network to drive business in assigned local market. • Ensure assigned team drives portfolio expansion, revenue growth, other performance targets, resulting in the maintenance of proper pipelines and customer engagement. Customers: • Drive the delivery of a customer-centric, relationship building approach to promote cross-sell of both business and personal products and services to business owners. • Drive up customer satisfaction and oversee the delivery of a positive customer experience by effectively meeting and exceeding expectations. • Focus on customer retention, in particular high value customers, by executing a consistent and effective client contact strategy. • Increase the cross-selling of TPP and Treasury products to deepen customer relationships and diversify Business Banking’s revenue steam. Leadership and Teamwork: • Lead and develop an effective team of RM/ Senior RMs through active communication, performance management, development planning, and reward/recognition practices. • Proactively train, coach, and provide guidance to team members, fostering a collaborative atmosphere. • Foster appropriate relationships with other business areas, steering staff behaviour to build partnerships in an aligned, collaborative manner, in order to present a diverse, tailored offering of products and services to our customers. • Ensure consistency of practices and compliance with pertinent policies and regulations.Major Challenges:• To independently lead a team who are responsible for attracting, retaining and growing a portfolio of customers during various economic and highly competitive environments. • To act in a sales advisory/coaching capacity, imparting an extensive knowledge base of the products and services offered within the Bank to effectively meet customer needs. • To manage customer expectation of service standards by effectively coordinating with internal partners to strictly adhere to agreed turnaround time.', '• Result Oriented • Collaborative & Co-ordinations Skills • Self-motivated and customer orientated • Sound problem solving and decision making skills • Good communication and presentation skills • Negotiation skills with ability to interact with people at various levels of the organization and outside environment. • Strong sales and relationship management skills with experience in corporate relationship management. • Must be a team player • Good knowledge of general banking/ RBI regulations, KYC/AML norms • Financial project evaluation, credit and analytical skills are a must • Knowledge of various Banking products.', '7-9 Yrs', 'The Best in the industry.'),
(8, 8, 'Assistant Manager / Manager Credit', 'C.A / CFA / MBA', '• To Manage credit processes and Credit Underwriting for Infrastructure Finance business • Administer Credit Policies for Infrastructure Finance division. • Credit Underwriting • Facilitate faster deal closures through proactive participation in deal structuring and credit enhancement. • Maintaining Turnaround Time for evaluation & processing of deals • Internal customer service - handling of other credit and compliance issues & taking corrective measures, being met with low TAT • Monitoring delinquency & corrective action - undertake regular interaction with collections / sales to track delinquencies • Close co-ordination with sales, collection and operations for business growth and portfolio health', '• Co-ordinations Skills • Analytical Skills • Self-motivated and customer orientated • Sound problem solving and decision making skills • Good communications and interpersonal skills • Computer literate in MS Office • Proficient in both spoken and written English', '2 – 4 years experience in Construction equipment Credit / Risk', 'The Best in the industry.'),
(9, 9, 'Manager / Senior Manager - Credit', 'C.A / CFA / MBA', '* Appraisal of Mid Corporate Proposals * Monitoring of existing portfolio. * Preparation of MIS. * Maintaining Turnaround Time for evaluation & processing of deals * Vendor management * Implement and administer Credit Policies for Commercial Finance * Credit Underwriting * Maintaining Turnaround Time for evaluation & processing of deals * Internal customer service - handling of other credit and compliance issues & taking corrective measures, being met with low TAT * Monitoring delinquency & corrective action - undertake regular interaction with collections / sales to track delinquencies * Regular Training sessions for Sales / credit Ops / CPA resources /Channel Partners / Filed sales staff on the various product policies & processes to ensure smooth functioning * Close co-ordination with sales, collection and operations for business growth and portfolio health', '* Collaborative & Co-ordinations Skills * Analytical Skills * Self-motivated and customer orientated * Sound problem solving and decision making skills * Good communications and interpersonal skills * Computer literate in MS Office * Proficient in both spoken and written English', '4 – 6 years experience in Mid Corporate Credit / Risk', 'The Best in the industry.'),
(10, 10, 'Regional Sales Manager – Project Finance: Infrastructure Finance', 'MBA/CA with 10 – 12 yrs of Experience . An engineering degree would be an added advantage', 'Team Management & its productivity, Sourcing, market presence.. * Team Management – Manage a team of relationship managers. Drive business through the team, monitor team productivity and keep them motivated. To work in close co-ordination with departments in related areas for smooth work flow, maintaining delivery schedules and drive business profitability & growth. * Relationship Management- Effective client relationship management to originate business & service client on required funding needs. Specifically, determine clients\' needs and devise financial solutions to meet their financial requirements. * Target Orientation - Exploring avenues for new income generation from new as well as existing clients. Achieving individual targets and growing account profitability, while maintaining a high service standard and compliance. * Product structuring - Adding value to the client and staying ahead of the competition by structuring and implementing innovative deals that suit the customer within acceptable risk levels. * Number of new accounts added for the region * Building the book * Build and maintain customers in infrastructure segment for various funding requirements * Maintaining the Net Interest Margin (NIM) * Fee Income * Maximizing share of wallet of the customer * Maintaining healthy portfolio – Zero delinquency', '* Good communication and presentation skills * Negotiation skills with ability to interact with people at various levels of the organization and outside environment. * Strong sales and relationship management skills with experience in corporate relationship management. * Must be a team player * Good credit knowledge for deal structuring * Good knowledge of general banking/ RBI regulations, KYC/AML norms * Financial project evaluation, credit and analytical skills are a must * Knowledge of various products including Trade Finance, Cash Management, Treasury and General Banking Product and Operations would be an added advantage. * Computer literate in MS Office * Proficient in both spoken and written English * Building Long-term Relations. * Develop a team of leaders * Analyze and improve work processes and policies. * Turn around time to be maintained.', 'Experience in Corporate finance / Project Finance / Infrastructure Finance', 'The Best in the industry.'),
(11, 11, 'Area Sales Manager', 'C.A / CFA / MBA', '* Sourcing of new customers, maintain the relations and build business * Health of the book, profitability, Productivity of the team * Sourcing / Business Development – New Acquisitions for Emerging / Mid-corporate * Create and Maintain Healthy and Profitable Book * Income Targets (including fee based) * Profitability Targets * To maintain existing customer base * Cross Sell both Corporate and Retail Products * MIS Reports. * Monitor and guide team members for the desired output / productivity * Provide advisory services to the key customers * Train the team for the necessary skill sets required for the role * Collection of over dues', '* Leadership qualities * Persuasiveness * Collaborative & Co-ordinations Skills * Analytical Skills * Self-motivated and customer orientated * Sound problem solving and decision making skills * Good communications and interpersonal skills * Computer literate in MS Office * Proficient in both spoken and written English', '6 – 8 years experience in Emerging / Mid-Corporate', 'The Best in the industry.'),
(12, 12, 'Regional Sales Manager – West', 'MBA/CFA', ' Responsible for the sales, collection, operations and central team co-ordination.   Target Achievement – incremental growth in the Book Size and maintain the health of the book .  Sourcing Tie ups: Tying up with relevant sourcing agents in the industry and build alternate sourcing channels .  Guiding the team for achieving the Business .  Team Management and Its Productivity .  Monitoring for the sales process and its effectiveness .  Fee Income .  Process Management .  Train and provide technical expertise to Relationship Managers / Field Sales Staff on the various Products .  Co-ordinating with the corporate team for restructuring products as per the specific regional need .  Cross-selling of products .  Maintain Nil NPA.', 'Team Handling Capabilities.   To establish relationship with competitors to have updated information about the changes happening in the industry.   Collaborative & Co-ordination Skills .  Progressive approach.   Analytical Skills .  Sound problem solving and decision making skills .  Good communications and interpersonal skills .  Proficient in both spoken and written English .  Proficient in both spoken and written English .  Persuasive, team motivator, customer orientated .  Sound problem solving and decision making skills .  Strong leadership including coaching skills .  Practical knowledge of Small, Mid Corporate products .  Strategic planning skills .  Turn around time to be maintained.', '9 – 12 years experience', 'The Best in the industry.'),
(13, 13, 'Area Credit Manager', 'C.A / CFA / MBA', '• To Implement & Manage credit processes and Credit Underwriting for Small Business & Supply chain Management • Appraisal & Credit Underwriting of SME / Equipment / Channel Finance Proposals • Monitoring of existing portfolio. • Preparation of MIS. • Maintaining Turnaround Time for evaluation & processing of deals • Vendor management • Implement and administer Credit Policies • Internal customer service - handling of other credit and compliance issues & taking corrective measures, being met with low TAT • Monitoring delinquency & corrective action - undertake regular interaction with collections / sales to track delinquencies • Regular Training sessions for Sales / credit Ops / CPA resources /Channel Partners / Filed sales staff on the various product policies & processes to ensure smooth functioning • Close co-ordination with sales, collection and operations for business growth and portfolio health', '• Collaborative & Co-ordinations Skills • Analytical Skills • Self-motivated and customer orientated • Sound problem solving and decision making skills • Good communications and interpersonal skills • Computer literate in MS Office • Proficient in both spoken and written English ', '3 – 5 years experience in SME / Equipment Finance / Channel finance – in Credit / Risk', 'The Best in the industry.'),
(14, 14, 'Senior Credit Analyst', 'C.A / CFA / MBA / CAIIB / ICWA', '• Credit underwriting , monitoring and credit management – Large corporate of CFD • Appraisal of Large Corporate Proposals • Monitoring of existing portfolio. • Preparation of MIS. • Maintaining Turnaround Time for evaluation & processing of deals • Vendor management • Implement and administer Credit Policies for Commercial Finance • Credit Underwriting • Client meeting, unit visit and client interaction. • Internal customer service - handling of other credit and compliance issues & taking corrective measures, being met with low TAT • Monitoring delinquency & corrective action – undertake regular interaction with collections / sales to track delinquencies • Regular Training sessions for Sales / credit Ops / CPA resources /Channel Partners / Filed sales staff on the various product policies & processes to ensure smooth functioning • Close co-ordination with sales, collection and operations for business growth and quality portfolio', '• Collaborative & Co-ordinations Skills • Analytical Skills • Self-motivated and customer orientated • Sound problem solving and decision making skills • Good communications and interpersonal skills • Computer literate in MS Office • Proficient in both spoken and written English', '7 – 9 years experience in Large Corporate Credit / Risk', 'The Best in the industry.'),
(15, 15, 'Regional Sales Manager: West – Commercial Finance', 'MBA / CFA', '* Responsible for the sales, collection, operations and central team co-ordination. * Target Achievement – incremental growth in the Book Size and maintain the health of the book * Sourcing Tie ups: Tying up with relevant sourcing agents in the industry and build alternate sourcing channels * Guiding the team for achieving the Business * Team Management and Its Productivity * Monitoring for the sales process and its effectiveness * Fee Income * Process Management * Train and provide technical expertise to Relationship Managers / Field Sales Staff on the various Products * Co-ordinating with the corporate team for restructuring products as per the specific regional need * Cross-selling of products * Maintain Nil NPA.', '* Team Handling Capabilities * To establish relationship with competitors to have updated information about the changes happening in the industry. * Collaborative & Co-ordination Skills * Progressive approach. * Analytical Skills * Sound problem solving and decision making skills * Good communications and interpersonal skills * Proficient in both spoken and written English * Persuasive, team motivator, customer orientated * Sound problem solving and decision making skills * Strong leadership including coaching skills * Practical knowledge of Small, Mid Corporate products * Strategic planning skills * Turn around time to be maintained.', '9 - 12 years', 'The Best in the industry.'),
(16, 16, 'Manager - Risk & Compliance', 'B.Com, M.Com, NCFM certified (Dealer Modules of Equity Cash & Derivatives, Currency Segments and Compliance)', '1. Should be able to manage risk for both offline and online trading of retail and institutional clients on BSE/NSE Cash & Derivatives segements. 2. Should possess hands-on experience in handling ODIN and TAW (Trade Any Where – online trading platform of DION Global earlier known as Asia DERC). 3. Would be responsible for managing day to day jobs like- a. Setting up of limits and surveillance b. Attending to and resolving branches/franchisees queries c. Selling of clients shares to recover client dues d. Squaring off of clients position in Derivatives Segment e. Ensuring strict adherence to policies & procedures f. Ensuring internal controls g. Tracking BSE/NSE/SEBI Circulars and ensure compliance h. Liaisoning with Exchange officials and software vendors for resolving issues', '1. Should be familiar with Margin Funding activities and relevant compliance norms 2. Possess thorough operating knowledge of Lidha-Didha back office software, E-Protector, LOTUS123, MS Word, Excel, PowerPoint etc. 3. Should be able to manage the team of 8-10 people 4. Should possess excellent communication skills both oral and written ', '4 - 6 years of experience in independently managing back office operations of Risk Management and Compliance.', 'The Best in the industry.'),
(17, 17, 'Sales Manager – CEQ (Infrastructure Finance)', 'Degree / MBA', '* To maintain a healthy book size and to build the same. * Responsible for marketing of Construction Equipment Finance * To disburse 60 Cr per annum. * Shall manage a Team of 4 Marketing Executives. * To take care of 0-60 DPD cases. * To help remedial in solving higher bucket cases. * To maintain healthy business relationship with customers, dealers and other associates. * To maintain constant vigil on competition and report their activities. * Ensure that all PDD are collected and the team follows all the rules & regulations strictly. * To ensure highly motivated Team is in place. * To train and monitor the Team.', '* Interpersonal Skills * Result Oriented * Collaborative & Co-ordinations Skills * Understanding & responsive * Self-motivated and customer orientated * Sound problem solving and decision making skills * Good communications and interpersonal skills', '3 to 5 years in Construction Equipments Industry', 'The Best in the industry.'),
(18, 18, 'Business Development Executive', 'B.Sc / B. Pharmacy / B.com /B. A.', 'Meeting Doctors, Chemists, Stockists on a regular intervals, achieving assigned sales target , timely reporting to the seniors.', 'Excellent Communication, Developing interpersonal business relations.', 'Minimum 1 Year Experience in MultiSpecialty Products.', 'Best in the Industry or at par with the leading Companies.'),
(19, 19, 'Business Development Executive', 'B.Sc / B. Pharmacy / B.com /B. A.', 'Meeting Doctors, Chemists, Stockists on a regular intervals, achieving assigned sales target , timely reporting to the seniors.', 'Excellent Communication, Developing interpersonal business relations.', 'Minimum 1 Year Experience in MultiSpecialty Products.', 'Best in the Industry or at par with the leading Companies.'),
(20, 20, 'Marketing Executive', 'B.Sc / B. Pharmacy', 'Meeting Doctors, Chemists, Stockists on a regular intervals, achieving assigned sales target , timely reporting to the seniors. ', 'Excellent Communication, Developing interpersonal business relations.', '• Below 27 years of Age. • Minimum 1 Year Experience in Cardio Diabetic & Pain Management products. • Preferably with the territory knowledge ', 'Best in the Industry or at par with the leading Companies.');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
