<?php
require_once 'db-config.php';
require_once 'classCandi.php';
if (isset($_POST['submit']))
{
    $jobID = $_GET['jobid'];
    $canName = $_POST['name'];
    $canEmail = $_POST['mail'];
    $canPhone = $_POST['phone'];
    $canRadio = $_POST['optradio'];
    //Upload file
    $fnm = "cv/";
    $cvDst = $fnm . basename($_FILES["cvFile"]["name"]);
    move_uploaded_file($_FILES["cvFile"]["tmp_name"], $cvDst);
    $obj = new Candi();
    $obj->storeInfo($jobID, $canName, $canEmail, $canPhone, $canRadio, $cvDst);
    echo '<script language="javascript">';
    echo 'alert("Submitted");';
    echo '</script>';
}
?>
<!DOCTYPE html>
<html>
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      <script src="js/jquery-3.5.1.min.js"></script>
      <title>TRP</title>
   </head>
   <body>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md">
               <div class="card">
                  <div class="card-header" style="background-color:#4872aa">
                     <img src="images/home_01copy.png" class="img-responsive" style="width:250px;">
                  </div>
                  <form action="" method="post" enctype="multipart/form-data" class="m-3" >
                     <div class="form-group">
                        <label for="job name">Job name:</label>
                        <input type="text" class="form-control" id="jobnm" value="<?php echo $_GET['jobnm'];?>" disabled>
                     </div>
                     <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" name="name" required>
                     </div>
                     <div class="form-group">
                        <label for="email">Email address:</label>
                        <input type="email" class="form-control" name="mail" required>
                     </div>
                     <div class="form-group">
                        <label for="phone">Enter a phone number:</label><br><br>
                        <input type="tel" id="phone" name="phone" placeholder="911234567890" pattern="[0-9]{10}" required><br><br>
                        <small>Format: 1234567890</small><br><br>
                     </div>
                     <label >Gender</label>
                     <div class="radio">
                        <label><input type="radio" name="optradio" value="m">Male</label>
                     </div>
                     <div class="radio">
                        <label><input type="radio" name="optradio" value="f">Female</label>
                     </div>
                     <div class="radio">
                        <label><input type="radio" name="optradio" value="o">Other</label>
                     </div>
                     <div class="custom-file mb-3">
                        <input type="file" class="custom-file-input" name="cvFile" required>
                        <label class="custom-file-label" for="customFile">Upload resume</label>
                     </div>
                     <button type="submit" class="btn btn-primary btn-lg" name="submit">Submit</button>
                     <button type="reset" class="btn btn-danger float-right btn-lg" >Reset</button>
                  </form>
               </div>   
            </div>
         </div>
      </div>
      <script>
         //code for the name of the file to appear on select
         $(".custom-file-input").on("change", function() {
           var fileName = $(this).val().split("\\").pop();
           $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
         });
      </script>
   </body>
</html>
<?php include 'footer.php'; ?>


