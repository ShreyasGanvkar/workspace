<?php
require_once "db-config.php";
class Db extends DBConfig
{
    protected $dbconn;
    public function __construct()
    {
        $this->dbconn = parent::dbConnect();
    }

    protected function fetchData($sql)
    {
        //error_log("Enter fetch data".$sql);
        $sql_stmt = $this->dbconn->prepare($sql);
        $sql_stmt->execute();
        $x = $sql_stmt->fetchAll(PDO::FETCH_ASSOC);
        return $x;
    }

    protected function iudData($sql)
    {	//error_log($sql);
        $sql_stmt = $this->dbconn->prepare($sql);
        $sql_stmt->execute();
    }

}
