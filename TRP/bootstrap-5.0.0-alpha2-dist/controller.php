<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
} else {
    header("Access-control-Allow-Origin: *");
}
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE, PUT");
    }

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    }

    exit(0);
}
require_once 'db-config.php';
require_once 'classIndustry.php';
require_once 'classRequire.php';
require_once 'classJob.php';

$action = $_GET['doaction'];

if ($action == 'getInd') {
    $obj = new Industry();
    $data = $obj->getIndustry();
} else if ($action == 'getReq') {
    $id = $_GET['oid'];
    $obj = new Requirement();
    $data = $obj->getReq($id);
} else if ($action = "getJob") {
    $jobid = $_GET['id'];
    $obj = new Job();
    $data = $obj->getJobs($jobid);
}
//ob_start();
//var_dump($data);
//error_log(ob_get_clean());
echo json_encode($data);
