<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="js/jquery-3.5.1.min.js"></script>
    <title>TRP</title>
  </head>
  <body>
  <div class="container-fluid mt-4">
    <div class="row">
        <div class="col-md-12">
        <div class="card ">
            <div class="card-header" style="background-color:#4872aa"><img src="images/home_01copy.png" class="img-responsive" style="width:250px;"> </div>
            <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody id="tableJob">
                            
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
            <script>
                 $(document).ready(function(){
                     var getid = <?php echo $_GET['jid'] ; ?> ;
                     //console.log(getid);
            	$.ajax({
            			type: "GET",
            			url: "http://localhost/myprojects/trp/bootstrap-5.0.0-alpha2-dist/controller.php?doaction=getJob&id="+getid,
            			data: {},
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",                    
                        cache: false,                       
                        success: function(response) 
            		        {	//console.log(response);
            				var tabjob = '';
            				$.each(response, function(i, job)
            					{ 	
            						tabjob += '<tr><td>Position</td><td>'+job.position+'</td></tr><tr><td>Qualifications</td><td>'+job.qualification+
                                    '</td></tr><tr><td>Deliverables</td><td>'+job.deliverables+'</td></tr><tr><td>Skills</td><td>'+job.skills+
                                    '</td></tr><tr><td>Experience</td><td>'+job.experience+'</td></tr><tr><td>Compensation</td><td>'+job.compensation+'</td></tr>';
            					});
            				//console.log(tabjob);	
            				$('#tableJob').html(tabjob);

            			},
            			
            		});
            });
            
            </script> 
        </div>
    </div>
    </div>  
  </body>
</html>
<?php include 'footer.php'; ?>