<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>API Test</title>
</head>
<body>
<h1 style="text-align:center">News</h1>
<div class="container" id="myData"> </div>


<script>
var url = 'http://newsapi.org/v2/top-headlines?' +
          'country=in&category=technology&' +
          'apiKey=9022e40d68b34efd8d3dc45f0cb5a235';
var req = new Request(url);
fetch(req)
    .then(abc =>abc.json()) 
    .then(xyz => {
            console.log(xyz);
            var mainContainer = document.getElementById("myData");
            xyz.articles.forEach(art => {
            var div = document.createElement("div");
            div.className = "card";
            div.setAttribute("style","margin-bottom:10px;padding:10px");

            var img = document.createElement("img");
            img.className = "img-fluid" ;
            img.src = ''+ art.urlToImage ;
            img.setAttribute("style","max-width:100%;height:auto" );

            div.innerHTML = '<b>Author:</b> ' + art.author + '<br><b>Content:</b> ' + art.content + '<br><b>Description:</b> ' + art.description ;
            mainContainer.appendChild(div);
            div.appendChild(img);
            //console.log(art.author);
        });
    });

</script>    