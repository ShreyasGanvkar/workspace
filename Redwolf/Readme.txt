The following files are included:

1.Assignment 1

  a)Assignment_1.pdf  - File describing the suggestions for improvement

2.Assignment 2

   src folder
  	-index.php         - Main file
  	-validate.php      - Validation and post submission file
  	-classCampaign.php - Class file
  	-config.php        - Database connection file
  	-style.css         - Stylesheet file
   db folder
	-redwolf.sql	   - SQL file to create table campaign  
   test folder
  	-TestPlan.xls      - Test plan for the form
        -Screenshot.pdf	   - Screenshots of desktop & mobile look and feel
			    (Mobile look is based on resizing the browser)

3.Installation instructions

   a)Copy all files from the Assignment 2 folder (excluding the TestPlan.xls) to the "www" folder.
   b)Create a new database called "redwolf" or use an existing database.
   c)Connect to the database and execute redwolf.sql.
   d)Change the hostname,dbname,username and password to match your environment.
   e)Launch the appropriate url of the web server where the contents of the src folder exist.
   f)Use TestPlan.xls for any testing. 

4.Form has been tested under Wampserver in IE, Firefox, Chrome.

   Environment   : Wampserver, VS Code, MySQl Workbench
   Browser tested: Firefox, Chrome, IE