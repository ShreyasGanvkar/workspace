<?php
require_once "config.php";
require_once "classCampaign.php";
$name_error = "";
$email_error = "";
$design_error = "";
$gender_error = "";
$color_error = "";
$date_error = "";
$camp = "";
$color = "";
$gender = "Male";
$output = "";
//error_log(print_r($_POST));
if (isset($_POST['submit'])) {
    //form post data
    $name = trim($_POST['fname']);
    $mail = trim($_POST['mail']);
    $desgttl = trim($_POST['desgtitle']);
    if (isset($_POST['gender'])) {
        $gender = $_POST['gender'];
    }
    if (isset($_POST['maleColor']) && $_POST['maleColor'] != "") {
        $color = $_POST['maleColor'];

    } else {
        $color = $_POST['femaleColor'];
    }

    $camp = $_POST['camp'];
    //Find the length of name and design title
    $nameLen = strlen($name);
    $designLen = strlen($desgttl);
    //name
    if (empty($name)) {
        $name_error = "Please enter Your Name!";
    } else if (!preg_match("/^([a-zA-Z' ]+)$/", $name)) {
        $name_error = "Please enter a Valid Name!";
    } else if ($nameLen < 3) {
        $name_error = "Name cannot be less than 3 letters!";
    } else if ($nameLen > 32) {
        $name_error = "Name cannot be more than 32 letters!";
    }
    //email
    if (empty($mail)) {
        $email_error = "Please enter Your E-Mail!";
    } else if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
        $email_error = "Enter a valid e-mail!";
    }
    //design title
    if (empty($desgttl)) {
        $design_error = "Please enter a Design Title!";
    } else if ($designLen < 3) {
        $design_error = "Design title cannot be less than 3 letters!";
    } else if ($designLen > 64) {
        $design_error = "Design title cannot be more than 64 letters!";
    }
    //gender
    if (empty($gender)) {
        $gender_error = "Please select your gender!";
    } else {
        $gender = $_POST['gender'];
    }
    //colour
    if (empty($color) || $color == "Select Tee Colour") {
        $color_error = "Please select colour!";
    }
    //date

    if ($camp == 1) {
        $date = date('d-M-Y');
    } else if ($camp == 0) {
        $date = $_POST['startdate'];
        //error_log("Date length--->".$date.strlen($date));
        if (empty($date)) {
            $date_error = "Please enter the date!";
        } else {
            $dateNow = new DateTime();
            $dateEntered = new DateTime($date);
            if ($dateEntered < $dateNow) {
                $date_error = "Date should be greater than today's date!";
            }
        }
    }
    //if no errors then save to database
    if ($name_error == "" && $email_error == "" && $design_error == "" && $gender_error == "" && $color_error == "" && $date_error == "") {

        $obj = new Campaign($conn);
        $obj->save($name, $mail, $desgttl, $gender, $color, $date);
        //display resulting information
        $myDate = DateTime::createFromFormat('d-M-Y', $date);
        $formattedDate = $myDate->format('d-M-Y');
        $output = '<h3><label>Campaign saved. Details as below!</label></h3>
            <table>
            <tr><td class="tabLeft">Name:</td><td> ' . $name . '</td></tr>
            <tr><td class="tabLeft">Email:</td><td> ' . $mail . '</td></tr>
            <tr><td class="tabLeft">Design Title:</td><td> ' . $desgttl . '</td></tr>
            <tr><td class="tabLeft">Gender:</td><td> ' . $gender . '</td></tr>
            <tr><td class="tabLeft">Colour:</td><td> ' . $color . '</td></tr>
            <tr><td class="tabLeft">Date:</td><td> ' . $formattedDate . '</td></tr>
            </table>   ';

        $name = "";
        $mail = "";
        $desgttl = "";
        $gender = "Male";
        $color = "";
        $camp = "";
    }
}