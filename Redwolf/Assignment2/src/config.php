<?php

	$host     = "localhost";
	$db_name  = "redwolf";
	$username = "root";
	$password = "";
	try {
		$conn = new PDO("mysql:host={$host};dbname={$db_name};charset=UTF8", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch (PDOException $exception) { 
		//to handle connection error
		echo "Connection error: " . $exception->getMessage();
		error_log($exception->getMessage());
	}

	
?>  